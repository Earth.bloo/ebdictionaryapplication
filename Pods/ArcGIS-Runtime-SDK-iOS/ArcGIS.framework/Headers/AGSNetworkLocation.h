/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>

@class AGSSourceObjectPosition;

/** @file AGSNetworkLocation.h */ //Required for Globals API doc

/** @brief References a specific location/position along a transportation network source feature.
 
 You need to pass this object to all network location functions. It represents an instance of a network location.
 
 @since 100
 */
@interface AGSNetworkLocation : AGSObject
NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

+(instancetype)networkLocation;

#pragma mark -
#pragma mark properties

/** Name of the network source.
 @since 100
 */
@property (nonatomic, copy, readwrite) NSString *sourceName;

/** True if the network element is on the right side of the network source, false - otherwise.
@since 100
*/
@property (nonatomic, assign, readwrite) BOOL onRightSideOfSource;

/** The position of the network element on the network source.
@since 100
*/
@property (nullable, nonatomic, strong, readwrite) AGSSourceObjectPosition *sourceObjectPosition;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END
@end
