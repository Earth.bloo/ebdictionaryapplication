/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityCategoryComparison.h */ //Required for Globals API doc

#import <ArcGIS/AGSUtilityTraceConditionalExpression.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityCategory;

/** An enumeration of the various types of operators to use when evaluating
 the existence of an @c AGSUtilityCategory on a network feature
 @see AGSUtilityCategoryComparison
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilityCategoryComparisonOperator) {
    AGSUtilityCategoryComparisonOperatorExists = 0,        /*!< Evaluate that the @c AGSUtilityCategory exists on a network feature */
    AGSUtilityCategoryComparisonOperatorDoesNotExist = 1   /*!< Evaluate that the @c AGSUtilityCategory does not exist on a network feature */
};

/** @brief A condition evaluating whether a particular @c AGSUtilityCategory exists on a feature in the trace

 @since 100.7
 */
@interface AGSUtilityCategoryComparison : AGSUtilityTraceConditionalExpression

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a condition evaluating whether an @c AGSUtilityCategory exists
 on a feature
 @since 100.7
 */
-(instancetype)initWithCategory:(AGSUtilityCategory *)category 
             comparisonOperator:(AGSUtilityCategoryComparisonOperator)comparisonOperator;

/** Creates a condition evaluating whether an @c AGSUtilityCategory exists
 on a feature
 @since 100.7
 */
+(instancetype)utilityCategoryComparisonWithCategory:(AGSUtilityCategory *)category 
                                  comparisonOperator:(AGSUtilityCategoryComparisonOperator)comparisonOperator;

#pragma mark -
#pragma mark properties

/** The category to evaluate as part of the condition
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityCategory *category;

/** The type of comparison to do between the @c AGSUtilityCategory and
 each feature in the trace
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityCategoryComparisonOperator comparisonOperator;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
