/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSAddRastersParameters.h>

/** @file AGSFrameCameraAddRastersParameters.h */ //Required for Globals API doc

/** @brief The frame camera parameters.
 
 An instance of this class represents frame camera parameters specifying which rasters to add to a mosaic dataset raster.

 @since 100
 */
@interface AGSFrameCameraAddRastersParameters : AGSAddRastersParameters

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize a frame camera add rasters parameters object to be used for adding rasters to a mosaic dataset raster.
 @since 100
 */
+(instancetype)frameCameraAddRastersParameters;

#pragma mark -
#pragma mark properties

/** URL to the input cameras file on disk.
 @since 100
 */
@property (nullable, nonatomic, strong, readwrite) NSURL *camerasFileURL;

/** URL to the input frames file on disk.
 @since 100
 */
@property (nullable, nonatomic, strong, readwrite) NSURL *framesFileURL;

/** Specifies whether to share raster info or not.
 @since 100
 */
@property (nonatomic, assign, readwrite) BOOL shareRasterInfo;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END

@end
