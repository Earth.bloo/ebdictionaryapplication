/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSPortalGroupContentSearchResultSet.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSPortalGroupContentSearchParameters;
@class AGSPortalItem;

/** @brief Results of a Group Content Search operation performed on a PortalGroup

 @see @c AGSPortalGroup#findItemsWithSearchParameters:completion:
 @since 100.7
 */
@interface AGSPortalGroupContentSearchResultSet : NSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Search parameters that can be used to fetch the next set of results
 This is automatically generated if there are more results to be retrieved, or it will be null if no more
 items are available. Pass it to a further call to @c AGSPortalGroup#findItemsWithSearchParameters:completion: to fetch the next set of
 results.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) AGSPortalGroupContentSearchParameters *nextSearchParameters;

/** The present results of the search
 These are limited by the value of @c AGSPortalGroupContentSearchParameters#limit in the parameters used to 
 generate these results.
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSArray<AGSPortalItem *> *results;

/** The search parameters used to generate these results
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSPortalGroupContentSearchParameters *searchParameters;

/** The total number of results irrespective of the paging
 @since 100.7
 */
@property (nonatomic, assign, readonly) NSInteger totalResults;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
