/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>
#import <ArcGIS/AGSJSONSerializable.h>

@class AGSSymbol;

/** @file AGSUniqueValue.h */ //Required for Globals API doc

/** @brief Each unique value that is symbolized differently by a unique value
 renderer.
 
 Instances of this class represent a unique value of matching attributes that must be symbolized by a unique value renderer.
 
 @since 100
 */


@interface AGSUniqueValue : AGSObject <AGSJSONSerializable, NSCopying>
NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize a unique value.
 @return A new unique value.
 @since 100
 */
+(instancetype)uniqueValue;

/** Initialize a new unique value (or unique combination of values) and a symbol used to display elements with this value in an @c AGSUniqueValueRenderer.
 @param description A description of the unique value. "Parcels zoned for residential use", for example.
 @param label A label for the unique value. "Residential", for example.
 @param symbol A symbol used to represent elements with this unique value.
 @param values An @c NSArray containing @c id types that define a unique value or unique combination of values.
 @return A new unique value.
 @since 100
 */
-(instancetype)initWithDescription:(NSString *)description
                             label:(NSString *)label
                            symbol:(AGSSymbol *)symbol
                            values:(NSArray<id> *)values;

/** Initialize a new unique value (or unique combination of values) and a symbol used to display elements with this value in an @c AGSUniqueValueRenderer.
@param description A description of the unique value. "Parcels zoned for residential use", for example.
@param label A label for the unique value. "Residential", for example.
@param symbol A symbol used to represent elements with this unique value.
@param values An @c NSArray containing @c id types that define a unique value or unique combination of values.
 @return A new unique value.
 @since 100
 */
+(instancetype)uniqueValueWithDescription:(NSString *)description
                                    label:(NSString *)label
                                   symbol:(AGSSymbol *)symbol
                                   values:(NSArray<id> *)values;

#pragma mark -
#pragma mark properties

/** A label for the unique value. "Residential", for example.
 @since 100
 */
@property (nonatomic, copy, readwrite) NSString *label;

/** A description of the unique value. "Parcels zoned for residential use", for example.
 @since 100
 */
@property (nonatomic, copy, readwrite) NSString *valueDescription;

/** A symbol used to represent elements with this unique value.
 @since 100
 */
@property (nullable, nonatomic, strong, readwrite) AGSSymbol *symbol;

/** An @c NSArray containing @c id types that define a unique value or unique combination of values.
 @since 100
 */
@property (nonatomic, copy, readwrite) NSArray<id> *values;

#pragma mark -
#pragma mark methods

/** Compares this unique value to another for equality.
 @param other unique value to compare this one to
 @return whether equal or not
 @since 100
 */
-(BOOL)isEqualToUniqueValue:(AGSUniqueValue*)other;

NS_ASSUME_NONNULL_END
@end
