/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>
#import <ArcGIS/AGSColor.h>

/** @file AGSBackgroundGrid.h */ //Required for Globals API doc

/** @brief A background grid defines the default color and context grid for display behind a map or scene surface.

The background grid determines what an empty @c AGSMapView or scene @c AGSSurface looks like. An instance of this class can
be used to set a default backdrop that an @c AGSMap or @c AGSScene will display on top of.
@see @c AGSMapView#backgroundGrid, @c AGSSurface#backgroundGrid, @c AGSMap#backgroundColor
@since 100.0
*/
@interface AGSBackgroundGrid : AGSObject
NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize the background grid with specified properties
 @param color Fill color of the background
 @param gridLineColor Color of background grid lines
 @param gridLineWidth Width (in points) of background grid lines
 @param gridSize Size (in points) of the background grid
 @return initialized background
 @since 100
 */
-(instancetype)initWithColor:(AGSColor*)color
               gridLineColor:(AGSColor*)gridLineColor
               gridLineWidth:(double)gridLineWidth
                    gridSize:(double)gridSize;

/** Initialize the background grid with specified properties
 @param color Fill color of the background
 @param gridLineColor Color of background grid lines
 @param gridLineWidth Width (in points) of background grid lines
 @param gridSize Size (in points) of the background grid
 @return initialized background
 @since 100
 */
+(instancetype)backgroundGridWithColor:(AGSColor*)color
                         gridLineColor:(AGSColor*)gridLineColor
                         gridLineWidth:(double)gridLineWidth
                              gridSize:(double)gridSize;
#pragma mark -
#pragma mark properties

/** The background color of the grid.
The default value is gray - RGBA(192,192,192,255).
 @since 100
 */
@property (nonatomic, strong, readwrite) AGSColor *color;

/** The color of background grid lines.
 The default value is black - RGBA(0,0,0,255).
 @since 100
 */
@property (nonatomic, strong, readwrite) AGSColor *gridLineColor;

/** The width (in points) of background grid lines.
 The default value is 0.1.
 @note This value must be greater than or equal to 0. Setting the grid line width to 0 will make grid lines invisible.
 @since 100
 */
@property (nonatomic, assign, readwrite) double gridLineWidth;

/** The size (in points) of the background grid.
 The default value is 20 x 20 (in points).
 @since 100
 */
@property (nonatomic, assign, readwrite) double gridSize;

/** A value indicating if grid lines are visible on the background.
 If this value is true, grid lines will display on top of the @c AGSBackgroundGrid#color.
 If this value is false, the @c AGSBackgroundGrid#color will display without the grid lines.
 The default value is true.
 @since 100.1
 */
@property (nonatomic, assign, readwrite, getter=isVisible) BOOL visible;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END
@end
