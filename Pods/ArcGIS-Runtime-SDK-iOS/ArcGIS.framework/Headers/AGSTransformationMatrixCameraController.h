/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSTransformationMatrixCameraController.h */ //Required for Globals API doc

#import <ArcGIS/AGSCameraController.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSCamera;
@class AGSTransformationMatrix;

/** @brief Support camera navigation by using @c AGSTransformationMatrix.
 
 @c AGSTransformationMatrixCameraController provides navigation by using
 @c AGSTransformationMatrix to control the camera's location and rotation.
 You need to pass this object to all @c AGSTransformationMatrixCameraController functions.
 This can be used with transformation matrices produced by AR APIs like ARKit and ARCore
 @see @c AGSOrbitLocationCameraController, @c AGSGlobeCameraController, @c AGSOrbitGeoElementCameraController
 @since 100.6
 */
@interface AGSTransformationMatrixCameraController : AGSCameraController

#pragma mark -
#pragma mark initializers

/** Create an @c AGSTransformationMatrixCameraController object.
 When the controller is set on the Scene View using @c AGSSceneView#cameraController the interaction mode
 will change for the active navigation model and be located at the origin camera's location
 and point along its rotation. The default camera has no rotation and is located at 0,0,15e6 meters.
 @since 100.6
 */
-(instancetype)init;

/** Create an @c AGSTransformationMatrixCameraController object.
 When the controller is set on the Scene View using @c AGSSceneView#cameraController the interaction mode
 will change for the active navigation model and be located at the origin camera's location
 and point along its rotation. The default camera has no rotation and is located at 0,0,15e6 meters.
 @since 100.6
 */
+(instancetype)transformationMatrixCameraController;

/** Create an @c AGSTransformationMatrixCameraController with an @c AGSCamera to describe the original location.
 When the controller is set on the Scene View using @c AGSSceneView#cameraController the interaction mode
 will change for the active navigation model and be located at the @c AGSTransformationMatrix's location
 and point along its rotation.
 @param originCamera All following movements will be relative to the origin camera's location.
 @since 100.6
 */
-(instancetype)initWithOriginCamera:(AGSCamera *)originCamera;

/** Create an @c AGSTransformationMatrixCameraController with an @c AGSCamera to describe the original location.
 When the controller is set on the Scene View using @c AGSSceneView#cameraController the interaction mode
 will change for the active navigation model and be located at the @c AGSTransformationMatrix's location
 and point along its rotation.
 @param originCamera All following movements will be relative to the origin camera's location.
 @since 100.6
 */
+(instancetype)transformationMatrixCameraControllerWithOriginCamera:(AGSCamera *)originCamera;

#pragma mark -
#pragma mark properties

/** Determines the clipping distance in meters around the @c AGSTransformationMatrixCameraController#originCamera. The default is 0.0.
 When the value is set to 0.0, there is no enforced clipping distance and therefore no limiting of displayed data.
 Setting the value to 10.0 will only render data 10 meters around the @c AGSTransformationMatrixCameraController#originCamera.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) double clippingDistance;

/** The @c AGSCamera that describes the original location. Setting a new @c AGSTransformationMatrix
 on the @c AGSTransformationMatrixCameraController will move the camera relative to the
 origin camera's location and orientation.
 @since 100.6
 */
@property (nonatomic, strong, readwrite) AGSCamera *originCamera;

/** The @c AGSTransformationMatrix describes the current @c AGSCamera's location relative to the origin camera.
 @since 100.6
 */
@property (nonatomic, strong, readwrite) AGSTransformationMatrix *transformationMatrix;

/** Defaults to 1.0. This value will be multiplied into the @c AGSTransformationMatrix property.
 Setting the value to 3 will cause position changes indicated by the transformation matrix
 property to be multiplied by 3. Note this does not affect @c AGSCamera rotation.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) double translationFactor;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
