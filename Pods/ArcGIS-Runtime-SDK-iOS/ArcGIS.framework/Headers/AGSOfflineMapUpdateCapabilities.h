/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSOfflineMapUpdateCapabilities.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Describes supported methods for obtaining updates for an offline map.
 
 @see @c AGSPreplannedMapArea#updateCapabilities
 @since 100.6
 */
@interface AGSOfflineMapUpdateCapabilities : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Whether an offline map supports downloading of read-only scheduled feature updates.
 If this property is true, updates are generated whenever the online map area is refreshed
 - for example according to its update schedule.
 
 Updates are prepared once and cached for download by all users of the offline map area.
 Sharing a single set of cached updates, rather than performing individual sync operations,
 reduces the load on the back-end services. This approach is scalable for large
 deployments. As updates are only downloaded, this approach can only be used with read-only
 workflows. The updates reflect a snapshot of the feature data at the time the online map
 area was refreshed. It does not reflect the most up-to-date feature data. For this approach
 the web map author must configure the online map to perform scheduled updates.
 
 If this property is false, no updates will be available for download.
 @since 100.6
 */
@property (nonatomic, assign, readonly) BOOL supportsScheduledUpdatesForFeatures;

/** Whether an offline map references feature services which are sync enabled.
 @since 100.6
 */
@property (nonatomic, assign, readonly) BOOL supportsSyncWithFeatureServices;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
