/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSKMLIconStyle.h */ //Required for Globals API doc

#import <ArcGIS/AGSKMLColorStyle.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSKMLIcon;
@class AGSKMLImageCoordinate;

/** @brief A KML icon style object.

 Specifies how icons for placemarks and photo overlays with a point geometry are drawn.
 Color is blended with the existing color of the icon.
 @since 100.6
 */
@interface AGSKMLIconStyle : AGSKMLColorStyle

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates an icon style with the specified icon and scale.
 @param icon Icon for drawing the point placemarks. If @c nil is passed, the default yellow pushpin icon will be used.
 @param scale Scale of the icon.
 @since 100.6
 */
-(instancetype)initWithIcon:(nullable AGSKMLIcon *)icon 
                      scale:(double)scale;

/** Creates an icon style with the specified icon and scale.
 @param icon Icon for drawing the point placemarks. If @c nil is passed, the default yellow pushpin icon will be used.
 @param scale Scale of the icon.
 @since 100.6
 */
+(instancetype)KMLIconStyleWithIcon:(nullable AGSKMLIcon *)icon 
                              scale:(double)scale;

#pragma mark -
#pragma mark properties

/** The direction in decimal degrees.
 Values range from 0 (North) to 360 degrees. The default value is 0.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) double heading;

/** The position within the icon that is anchored to the point specified in the placemark.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLImageCoordinate *hotSpot;

/** The icon to be used by the placemark. If @c nil is set, the default yellow pushpin icon will be used.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLIcon *icon;

/** The scale factor that should be applied to the rendered icon.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) double scale;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
