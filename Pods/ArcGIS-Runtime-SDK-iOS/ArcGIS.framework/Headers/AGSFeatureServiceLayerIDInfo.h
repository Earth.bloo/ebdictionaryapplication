/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSFeatureServiceLayerIDInfo.h */ //Required for Globals API doc

#import <ArcGIS/AGSIDInfo.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief An object that represents the metadata for a Feature service layer.
 
 Same as an @c AGSIDInfo.
 You need to pass this object to all feature service layer id info functions. You will get this object from
 @c AGSArcGISFeatureServiceInfo#layerInfos.
 @see @c AGSArcGISFeatureServiceInfo#layerInfos
 @since 100.6
 */
@interface AGSFeatureServiceLayerIDInfo : AGSIDInfo

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Whether the features in the layer should be visible when loaded.
 @since 100.6
 */
@property (nonatomic, assign, readonly) BOOL defaultVisibility;

/** The type of geometry that all the features in the layer possess.
 For point, multipoint, polyline or polygon feature layers, this will be the type of the feature's geometry.
 For annotation feature layers, this will be @c AGSGeometryTypePolygon
 (referring to the outline of the text graphic).
 For tables, this will be @c AGSGeometryTypeUnknown.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSGeometryType geometryType;

/** The maximum scale at which the layer's contents will be visible.
 The layer is only visible when you are zoomed further out than the maxScale.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double maxScale;

/** The minimum scale at which the layer's contents will be visible.
 The layer is only visible when you are zoomed further in than the minScale. However, zero indicates that there is no minScale restriction.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double minScale;

/** The type of data held in the layer on the service.
 Indicates the type of data contained in the layer
 e.g. table row, geometric feature or annotation feature.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSArcGISFeatureLayerInfoServiceType serviceType;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
