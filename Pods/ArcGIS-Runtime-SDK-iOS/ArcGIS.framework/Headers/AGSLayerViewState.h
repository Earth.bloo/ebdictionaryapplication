/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/*@file AGSLayerViewState.h */

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/**
 The status could be a combination of any of these individual states.
 @since 100
 */
typedef NS_OPTIONS(NSInteger, AGSLayerViewStatus) {
    AGSLayerViewStatusActive = 1 << 0,      /*!< The layer in the view is active. Is exclusive and will never be reported together with other statuses. */
    AGSLayerViewStatusNotVisible = 1 << 1,  /*!< The layer in the view is not visible. */
    AGSLayerViewStatusOutOfScale = 1 << 2,  /*!< The layer in the view is out of scale. */
    AGSLayerViewStatusLoading = 1 << 3,     /*!< The layer in the view is loading. */
    AGSLayerViewStatusError = 1 << 4        /*!< The layer in the view has an error. */
};

/** @brief The view state of a layer
 
 Instances of this class represent the view state of a layer as it is being displayed on screen. 
 
 @since 100
 */
@interface AGSLayerViewState : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The error, if any, preventing the layer from being displayed on screen
 @since 100
 */
@property (nullable, nonatomic, strong, readonly) NSError *error;

/** The status of how the layer is being displayed on screen
 @since 100
 */
@property (nonatomic, assign, readonly) AGSLayerViewStatus status;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
