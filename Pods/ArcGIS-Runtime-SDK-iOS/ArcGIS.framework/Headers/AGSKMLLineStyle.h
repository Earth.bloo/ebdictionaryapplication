/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSKMLLineStyle.h */ //Required for Globals API doc

#import <ArcGIS/AGSKMLColorStyle.h>
#import <ArcGIS/AGSColor.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief The drawing style (color, color mode, and line width) for all line geometry.
 
 Specifies the drawing style (color, color mode, and line width) for all line geometry. Line geometry includes
 the outlines of outlined polygons and the extruded "tether" of Placemark icons (if extrusion is enabled).
 @since 100.6
 */
@interface AGSKMLLineStyle : AGSKMLColorStyle

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a KML line style with a specified line color and width.
 @param color The KML line style's color.
 @param width of the KML line style, in pixels.
 @since 100.6
 */
-(instancetype)initWithColor:(AGSColor *)color
                       width:(double)width;

/** Creates a KML line style with a specified line color and width.
 @param color The KML line style's color.
 @param width of the KML line style, in pixels.
 @since 100.6
 */
+(instancetype)KMLLineStyleWithColor:(AGSColor *)color
                               width:(double)width;

#pragma mark -
#pragma mark properties

/** Width of the KML line style, in pixels.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) double width;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END

