/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSRouteTracker.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSLocation;
@class AGSRouteParameters;
@class AGSRouteResult;
@class AGSRouteTask;
@class AGSTrackingStatus;
@class AGSVoiceGuidance;

@protocol AGSCancelable, AGSRouteTrackerDelegate;

/** @brief Uses a location to provide status and progress updates as a route is traversed (by a moving vehicle, for example).
 
 @c AGSRouteTracker can give the time or distance to the next maneuver, notify if the location is off-route, and regenerate a new route if
 necessary.
 Basic workflow:
 1. Create a new @c AGSRouteTracker instance.
 2. Enable rerouting with @c AGSRouteTracker#enableReroutingWithRouteTask:routeParameters:strategy:visitFirstStopOnStart:completion: (if supported by the underlying route service).
 3. Use @c AGSRouteTracker#trackLocation:completion: to track the location of the device.
 4. Implement the delegate method @c AGSRouteTrackerDelegate#routeTracker:didUpdateTrackingStatus: to get the @c AGSTrackingStatus as the location changes.
 5. Implement the delegate method @c AGSRouteTrackerDelegate#routeTracker:didGenerateNewVoiceGuidance: to get the @c AGSVoiceGuidance whenever new instructions are available.
 6. If there are multiple stops, call @c AGSRouteTracker#switchToNextDestinationWithCompletion: each time @c AGSDestinationStatusReached status is returned.
 Before calling @c AGSRouteTracker#switchToNextDestinationWithCompletion: make sure that @c AGSTrackingStatus#remainingDestinationCount > 1,
 a value of 1 means the last destination is being approached.
 @since 100.6
 */
@interface AGSRouteTracker : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a Route Tracker
 Will return @c nil if the @c AGSRoute is not found or if it lacks Stops or Directions.
 @param routeResult A @c AGSRouteResult object obtained by calling @c AGSRouteTask#solveRouteWithParameters:completion:
 @param routeIndex The zero-based index of the @c AGSRoute in @c AGSRouteResult to track
 @see @c AGSRouteResult
 @since 100.6
 */
-(nullable instancetype)initWithRouteResult:(AGSRouteResult *)routeResult
                                 routeIndex:(NSInteger)routeIndex;

/** Creates a Route Tracker
 Will return @c nil if the @c AGSRoute is not found or if it lacks Stops or Directions.
 @param routeResult A @c AGSRouteResult object obtained by calling @c AGSRouteTask#solveRouteWithParameters:completion:
 @param routeIndex The zero-based index of the @c AGSRoute in @c AGSRouteResult to track
 @see @c AGSRouteResult
 @since 100.6
 */
+(nullable instancetype)routeTrackerWithRouteResult:(AGSRouteResult *)routeResult
                                         routeIndex:(NSInteger)routeIndex;

#pragma mark -
#pragma mark properties

/** The delegate that receives callbacks for the @c AGSRouteTracker.
 @see @c AGSRouteTrackerDelegate
 @since 100.6
 */
@property (nullable, nonatomic, weak, readwrite) id<AGSRouteTrackerDelegate> delegate;

/** Reports if routes are automatically regenerated when tracking status is off-route. True if rerouting is enabled, otherwise False
 @see @c AGSRouteTracker#enableReroutingWithRouteTask:routeParameters:strategy:visitFirstStopOnStart:completion:, @c AGSRouteTracker#disableRerouting
 @since 100.6
 */
@property (nonatomic, assign, readonly, getter=isReroutingEnabled) BOOL reroutingEnabled;

/** Status for the current location on the route
 @see @c AGSTrackingStatus
 @since 100.6
 */
@property (nullable, nonatomic, strong, readonly) AGSTrackingStatus *trackingStatus;

/** The unit system used in voice guidance commands
 Default value is @c AGSUnitSystemMetric. Supported values are @c AGSUnitSystemMetric and @c AGSUnitSystemImperial.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) AGSUnitSystem voiceGuidanceUnitSystem;

#pragma mark -
#pragma mark methods

/** Cancels a running reroute operation.
 If rerouting is in progress, this method will interrupt the background route task operation.
 @since 100.6
 */
-(void)cancelRerouting;

/** Disables automatic rerouting.
 When disabled, the tracker will not automatically recalculate a route when the tracking status is off-route.
 @see @c AGSRouteTracker#enableReroutingWithRouteTask:routeParameters:strategy:visitFirstStopOnStart:completion:
 @since 100.6
 */
-(void)disableRerouting;

/** Enables automatic rerouting when the tracker detects an off-route status.
 Rerouting is initiated automatically when the tracking status is off-route. In order to be considered off-route,
 the location must be on the transportation network as well as off the current route. If a tracked location is in
 a parking lot, for example, it is not considered off-route and rerouting will not occur. If the next location is
 on the network but not on the route, automatic rerouting will begin.
 @param routeTask An @c AGSRouteTask capable of solving routes on the same network used by the original route.
 @param routeParameters An @c AGSRouteParameters object that defines parameters for the rerouting @c AGSRouteTask.
 @param strategy A rerouting strategy that determines how new routes are created (to the next waypoint by default).
 @param visitFirstStopOnStart Whether the first stop must be visited when rerouted (false by default).
 @param completion The #error parameter is populated on failure.
 @return An operation which can be canceled.
 @see @c AGSRouteTask, @c AGSRouteParameters, @c AGSReroutingStrategy
 @since 100.6
 */
-(id<AGSCancelable>)enableReroutingWithRouteTask:(AGSRouteTask *)routeTask 
                                 routeParameters:(AGSRouteParameters *)routeParameters
                                        strategy:(AGSReroutingStrategy)strategy
                           visitFirstStopOnStart:(BOOL)visitFirstStopOnStart
                                      completion:(void(^)(NSError * __nullable error))completion;

/** Gets the current @c AGSVoiceGuidance object.
 The most recent voice guidance based on the last @c AGSLocation used by @c AGSRouteTracker#trackLocation:completion:.
 It can be used to repeat last/latest voice guidance.
 @return Voice guidance.
 @see @c AGSTrackingStatus
 @since 100.6
 */
-(nullable AGSVoiceGuidance *)generateVoiceGuidance;

/** Starts tracking progress to the next destination in the @c AGSRoute.
 Use @c AGSRouteTracker#switchToNextDestinationWithCompletion: when the tracker reports an @c AGSDestinationStatusReached status.
 Before calling @c AGSRouteTracker#switchToNextDestinationWithCompletion: make sure that @c AGSTrackingStatus#remainingDestinationCount > 1
 (a value of 1 means navigation is proceeding to last destination). This method can also be called after
 @c AGSDestinationStatusApproaching is raised in cases where the location cannot get near enough to the destination point
 for @c AGSDestinationStatusReached to be raised (such as the center of a park or shopping center, for example).
 The delegate is called when the @c AGSTrackingStatus is updated accordingly.
 @param completion The #error parameter is populated on failure.
 @return An operation which can be canceled.
 @since 100.6
 */
-(id<AGSCancelable>)switchToNextDestinationWithCompletion:(nullable void(^)(NSError * __nullable error))completion;

/** Provides route tracking status relative to the provided location.
 The location generally comes from a GPS but may also be a simulated or manually entered location.
 It must have valid values for X and Y coordinates, speed (in meters per second), course (in degrees),
 and a timestamp.
 The delegate is called when the @c AGSTrackingStatus is updated for this location.
 @param location A location used to evaluate status and progress along the route.
 @param completion A block that is invoked when the operation finishes. The #error parameter is populated on failure.
 @return The operation which can be canceled
 @see @c AGSLocation
 @since 100.6
 @license{Basic}
 @ingroup licensing
 */
-(id<AGSCancelable>)trackLocation:(AGSLocation *)location 
                       completion:(nullable void(^)(NSError * __nullable error))completion;

@end

/**
 Methods for managing route tracker changes.
 @since 100.6
 */
@protocol AGSRouteTrackerDelegate <NSObject>

@optional

/**
 Tells the delegate that the route tracker has a new voice guidance available
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @param voiceGuidance The new voice guidance object that was generated.
 @since 100.6
 */
-(void)routeTracker:(AGSRouteTracker*)routeTracker
didGenerateNewVoiceGuidance:(AGSVoiceGuidance*)voiceGuidance;


/**
 Tells the delegate that the route tracker started re-routing.
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @since 100.6
 */
-(void)routeTrackerRerouteDidStart:(AGSRouteTracker*)routeTracker;


/**
 Tells the delegate that the route tracker completed re-routing.
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @param trackingStatus The tracking status after the re-route completed.
 @param error The error if the re-route failed.
 @since 100.6
 */
-(void)routeTracker:(AGSRouteTracker*)routeTracker
rerouteDidCompleteWithTrackingStatus:(nullable AGSTrackingStatus*)trackingStatus
              error:(nullable NSError*)error;


/**
 Tells the delegate that the route tracker has updated it's tracking status.
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @param trackingStatus The latest tracking status.
 @since 100.6
 */
-(void)routeTracker:(AGSRouteTracker*)routeTracker
didUpdateTrackingStatus:(AGSTrackingStatus*)trackingStatus;

@end

NS_ASSUME_NONNULL_END
