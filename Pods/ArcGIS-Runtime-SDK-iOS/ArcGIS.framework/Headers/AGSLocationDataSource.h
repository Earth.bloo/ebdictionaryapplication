/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>

@class AGSLocation;
@class AGSPoint;
@protocol AGSLocationChangeHandlerDelegate;

NS_ASSUME_NONNULL_BEGIN

/** @file AGSLocationDataSource.h */

/** @brief An abstract base class that provides location updates to @c AGSLocationDisplay
 
 Subclasses represent datasources that provide location updates to the mapview's location display.
 
 @since 100
 @see AGSLocationDisplay
 */

@interface AGSLocationDataSource : AGSObject

#pragma mark -
#pragma mark initializers


#pragma mark -
#pragma mark properties

/** Indicates whether the datasource is active or not
 @since 100
 */
@property (nonatomic, assign, readonly) BOOL started;

/** The error that prevented the datasource from starting or was encountered while retrieving a location udpate
 @since 100
 */
@property (nullable, nonatomic, strong, readonly) NSError *error;

/** The delegate which will receive location, heading and status updates from the data source.
 @since 100.6
 */
@property (nullable, nonatomic, weak, readwrite) id<AGSLocationChangeHandlerDelegate> locationChangeHandlerDelegate;

#pragma mark -
#pragma mark methods

/** This is invoked by the location display on the datasource to initiate requesting location updates. Internally this method calls `#doStart` which subclasses must implement.
 @param completion block which will be invoked when the operation completes. If the dataSource failed to start, the error property will be populated.
 @since 100
 */
-(void)startWithCompletion:(nullable void(^)(NSError *__nullable error))completion;

/** This is invoked by the location display on the datasource to stop requesting location updates. Internally this method calls `#doStop` which subclasses must implement.
 @since 100
 */
-(void)stop;

@end

/** @brief A category to organize subclassable aspects of @c AGSLocationDataSource
 
 Members of particular interest while subclassing @c AGSLocationDataSource
 
 @since 100
 */
@interface AGSLocationDataSource (ForSubclassEyesOnly)

/** Subclasses must implement this method to start the datasource.
 Once the datasource has started or failed to start it should call `#didStartOrFailWithError:`. As updates are received, the datasource should call `#didUpdateLocation:` or `#didUpdateHeading:`.
 @since 100
 */
-(void)doStart;

/** Subclasses must implement this method to stop the datasource.
 Once the datasource has stopped it should call `#didStop`
 @since 100
 */
-(void)doStop;

/** Subclasses must call this in `#doStart` once the datasource has started.
 @since 100
 */
-(void)didStartOrFailWithError:(nullable NSError*)error;

/** Subclasses must call this in `#doStop` once the datasource has stopped.
 @since 100
 */
-(void)didStop;

/** Subclasses must call this once they have a new heading.
 @since 100
 */
-(void)didUpdateHeading:(double)heading;

/** Subclasses must call this once they have a new location.
 @since 100
 */
-(void)didUpdateLocation:(AGSLocation*)location;

@end

#pragma mark -
#pragma mark Location Change Handler Delegate Methods
#pragma mark -

/** @brief A change handler delegate for @c AGSLocationDataSource.
 
 A protocol which must be adopted by a class wishing to informed about location and heading changed events on @c AGSLocationDatasource. An instance of the
 class must then be set as @c AGSLocationDataSource#locationChangeHandlerDelegate.
 
 
 All of the methods of this protocol are optional.
 
 @define{AGSLocationDatasource.h, ArcGIS}
 @since 100.6
 */
@protocol AGSLocationChangeHandlerDelegate <NSObject>
@optional

/** Tells the delegate that the location has changed.
 @param locationDataSource The location data source causing the location change.
 @param location The new location.
 @since 100.6
 */
- (void)locationDataSource:(AGSLocationDataSource *)locationDataSource locationDidChange:(AGSLocation *)location;

/** Tells the delegate that the heading has changed.
 @param locationDataSource The location data source causing the heading change.
 @param heading The new heading.
 @since 100.6
 */
- (void)locationDataSource:(AGSLocationDataSource *)locationDataSource headingDidChange:(double)heading;

/** Tells the delegate that the data source status has changed.
 @param locationDataSource The location data source causing the location change.
 @param status The new status.
 @since 100.6
 */
- (void)locationDataSource:(AGSLocationDataSource *)locationDataSource statusDidChange:(AGSLocationDataSourceStatus)status;

@end

NS_ASSUME_NONNULL_END
