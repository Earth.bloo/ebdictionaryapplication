/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSCancelable.h */ //Required for Globals API doc

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief A protocol exposed by operations that allow cancellation
 
 A protocol exposed by operations that allow cancellation while in progress.
 
 @define{AGSCancelable.h, ArcGIS}
 @since 100
 */
@protocol AGSCancelable <NSObject>

@required

/** Cancel the operation
 @since 100
 */
- (void)cancel;

/** Indicates whether the operation has been canceled
 @since 100
 */
- (BOOL)isCanceled;

@end

NS_ASSUME_NONNULL_END
