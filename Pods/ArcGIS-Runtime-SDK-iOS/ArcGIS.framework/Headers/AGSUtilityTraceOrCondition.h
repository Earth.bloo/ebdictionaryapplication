/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTraceOrCondition.h */ //Required for Globals API doc

#import <ArcGIS/AGSUtilityTraceConditionalExpression.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Two sub-expressions combined together using a logical OR operator

 @since 100.7
 */
@interface AGSUtilityTraceOrCondition : AGSUtilityTraceConditionalExpression

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a logical OR operator of two sub-expressions
 @param leftExpression The left sub-expression
 @param rightExpression The right sub-expression
 @since 100.7
 */
-(instancetype)initWithLeftExpression:(AGSUtilityTraceConditionalExpression *)leftExpression 
                      rightExpression:(AGSUtilityTraceConditionalExpression *)rightExpression;

/** Creates a logical OR operator of two sub-expressions
 @param leftExpression The left sub-expression
 @param rightExpression The right sub-expression
 @since 100.7
 */
+(instancetype)utilityTraceOrConditionWithLeftExpression:(AGSUtilityTraceConditionalExpression *)leftExpression 
                                         rightExpression:(AGSUtilityTraceConditionalExpression *)rightExpression;

#pragma mark -
#pragma mark properties

/** The left sub-expression
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityTraceConditionalExpression *leftExpression;

/** The right sub-expression
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityTraceConditionalExpression *rightExpression;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
