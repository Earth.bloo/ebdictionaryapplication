/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSKMLImageCoordinate.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief A KML image coordinate object.
 
 Instances of this class represent a KML image coordinate object as described in the OGC 2.3 specification, section 16.26.2 (http://docs.opengeospatial.org/is/12-007r2/12-007r2.html#1254).
 
 It specifies an image coordinate system. The x and y values may each be specified in three different ways - as pixels (pixels), as
 fractions of the icon (fraction), or as inset pixels (insetPixels), which is an offset in pixels from the upper right corner of
 the icon. They may or may not be specified in a consistent manner - for example, x can be specified in pixels and y as a fraction.
 
 Use the @c AGSKMLUnitsType enumeration (either via the @c AGSKMLImageCoordinate#xUnits and @c AGSKMLImageCoordinate#yUnits properties or
 via the @c AGSKMLImageCoordinate#initWithX:y:xUnits:yUnits: constructor) to establish the KML
 image coordinate system.
 @since 100.6
 */
@interface AGSKMLImageCoordinate : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a KML image coordinate.
 @param x The x component of a point.
 @param y The y component of a point.
 @param xUnits Units in which the x value is specified.
 @param yUnits Units in which the y value is specified.
 @since 100.6
 */
-(instancetype)initWithX:(double)x
                       y:(double)y
                  xUnits:(AGSKMLUnitsType)xUnits
                  yUnits:(AGSKMLUnitsType)yUnits;

/** Creates a KML image coordinate.
 @param x The x component of a point.
 @param y The y component of a point.
 @param xUnits Units in which the x value is specified.
 @param yUnits Units in which the y value is specified.
 @since 100.6
 */
+(instancetype)KMLImageCoordinateWithX:(double)x
                                     y:(double)y
                                xUnits:(AGSKMLUnitsType)xUnits
                                yUnits:(AGSKMLUnitsType)yUnits;

#pragma mark -
#pragma mark properties

/** The x component of a point.
 The default value is @c 1.0.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double x;

/** Units in which the x value is specified.
 The default value is @c AGSKMLUnitsTypeFraction.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSKMLUnitsType xUnits;

/** The y component of a point.
 The default value is @c 1.0.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double y;

/** Units in which the y value is specified.
 The default value is @c AGSKMLUnitsTypeFraction.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSKMLUnitsType yUnits;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
