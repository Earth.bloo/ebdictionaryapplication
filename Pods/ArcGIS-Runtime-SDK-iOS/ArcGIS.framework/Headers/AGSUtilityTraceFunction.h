/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTraceFunction.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

@class AGSUtilityNetworkAttribute;

NS_ASSUME_NONNULL_BEGIN

/** The type of function calculation to perform.
 @see @c AGSUtilityTraceFunction
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilityTraceFunctionType) {
    AGSUtilityTraceFunctionTypeAdd = 1,       /*!< Computes the sum of the network attribute from each applicable element. */
    AGSUtilityTraceFunctionTypeAverage = 2,   /*!< Computes the average value of the network attribute from each applicable element. */
    AGSUtilityTraceFunctionTypeCount = 3,     /*!< Counts the number of applicable elements. */
    AGSUtilityTraceFunctionTypeMax = 4,       /*!< Computes the maximum value of the network attribute from each applicable element. */
    AGSUtilityTraceFunctionTypeMin = 5,       /*!< Computes the minimum value of the network attribute from each applicable element. */
    AGSUtilityTraceFunctionTypeSubtract = 6   /*!< Takes the network attribute value from the starting point as the base number, and then subtracts the value of the network attribute from each applicable element. */
};

/** @brief Functions allow the computation of values during a network trace.

 Functions are evaluated at each applicable network element. The meaning of applicable varies depending on the Tracer.
 For an upstream trace, the functions are evaluated for each upstream element.
 For a downstream trace, the functions are evaluated for each element in the subnetwork.
 @since 100.7
 */
@interface AGSUtilityTraceFunction : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a function object to compute a value from a network attribute.
 @param functionType The calculation to perform.
 @param networkAttribute The @c AGSUtilityNetworkAttribute to use with the calculation.
 @since 100.7
 */
-(instancetype)initWithFunctionType:(AGSUtilityTraceFunctionType)functionType 
                   networkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute;

/** Creates a function object to compute a value from a network attribute.
 @param functionType The calculation to perform.
 @param networkAttribute The @c AGSUtilityNetworkAttribute to use with the calculation.
 @since 100.7
 */
+(instancetype)utilityTraceFunctionWithFunctionType:(AGSUtilityTraceFunctionType)functionType 
                                   networkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute;

#pragma mark -
#pragma mark properties

/** The calculation to perform.
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityTraceFunctionType functionType;

/** The @c AGSUtilityNetworkAttribute to use with the calculation.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) AGSUtilityNetworkAttribute *networkAttribute;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
