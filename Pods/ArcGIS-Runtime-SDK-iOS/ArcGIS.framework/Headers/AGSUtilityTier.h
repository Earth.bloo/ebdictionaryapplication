/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTier.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityTraceConfiguration;

/** Specifies the type of topology that makes up an @c AGSUtilityTier definition
 @see @c AGSUtilityTier
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilityTierTopologyType) {
    AGSUtilityTierTopologyTypeRadial = 1,  /*!< A radial network. Edges and junctions spread out radially from a single source. */
    AGSUtilityTierTopologyTypeMesh = 2     /*!< A mesh network. Edges and junctions are connected to multiple sources in an interconnected lattice. */
};

/** @brief Tiers demarcate a logical level within a network

 For example, in an electric distribution network, there may be Subtransmission, Medium Voltage and Low Voltage tiers.
 @since 100.7
 */
@interface AGSUtilityTier : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The name of the @c AGSUtilityTier
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSString *name;

/** The numeric rank of the tier
 It is possible for several tiers to have the same numeric rank. 
 For example, in an electric distribution network, Radial Medium Voltage and Radial Multifeed Medium Voltage tiers would have the same rank.
 When doing a trace between a source tier and a target tier, the source and target tiers define a upper and lower tier value range for a trace. 
 The trace will stop if it encounters a tier whose rank is outside the range specified by the source and target tiers.
 @since 100.7
 */
@property (nonatomic, assign, readonly) NSInteger rank;

/** The @c AGSUtilityTierTopologyType of the tier
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityTierTopologyType topologyType;

/** The default @c AGSUtilityTraceConfiguration that defines subnetworks within this tier
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) AGSUtilityTraceConfiguration *traceConfiguration;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
