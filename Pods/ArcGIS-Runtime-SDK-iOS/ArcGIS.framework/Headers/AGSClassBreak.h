/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>
#import <ArcGIS/AGSJSONSerializable.h>

@class AGSSymbol;

/** @file AGSClassBreak.h */ //Required for Globals API doc

/** @brief A class break object used to categorize a group of values that fall within a range of values.
 
 The class break is used to categorize a group of values that fall within a range of `minValue` and `maxValue`
 settings. When defining this categorization group, the value to be categorized must be greater than the
 `minValue` but less than or equal to the `maxValue`. When written as an algebraic equation, it would look like
 (`minValue` < value <= `maxValue`).
 The @c AGSClassBreaksRenderer applies the associated symbol to features or graphics which have
 values that fall within the specified range.
 @since 100
 */
@interface AGSClassBreak : AGSObject <AGSJSONSerializable, NSCopying>
NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize an <code>AGSClassBreak</code>.
 @param description A description of the class break. "Cities with a population under 100,000", for example.
 @param label A label for the class break. "0 - 100000", for example.
 @param minValue The minimum value of the range that defines the break.
 @param maxValue The maximum value of the range that defines the break.
 @param symbol A symbol used to represent elements in the class break.
 @return A new class break
 @since 100
 */
- (instancetype)initWithDescription:(NSString *)description
                              label:(NSString *)label
                           minValue:(double)minValue
                           maxValue:(double)maxValue
                             symbol:(AGSSymbol *)symbol;

/** Initialize an <code>AGSClassBreak</code>.
 @return A new class break
 @since 100
 */
+(instancetype)classBreak;

/** Initialize an <code>AGSClassBreak</code>.
 @param description A description of the class break. "Cities with a population under 100,000", for example.
 @param label A label for the class break. "0 - 100000", for example.
 @param minValue The minimum value of the range that defines the break.
 @param maxValue The maximum value of the range that defines the break.
 @param symbol A symbol used to represent elements in the class break.
 @return A new class break
 @since 100
 */
+(instancetype)classBreakWithDescription:(NSString *)description
                                   label:(NSString *)label
                                minValue:(double)minValue
                                maxValue:(double)maxValue
                                  symbol:(AGSSymbol *)symbol;

#pragma mark -
#pragma mark properties

/** A label for the class break. "0 - 100000", for example.
 @since 100
 */
@property (nonatomic, copy, readwrite) NSString * label;

/** A description of the class break. "Cities with a population under 100,000", for example.
 @since 100
 */
@property (nonatomic, copy, readwrite) NSString * breakDescription;

/** The minimum value of the range that defines the break.
 @since 100
 */
@property (nonatomic, assign, readwrite) double minValue;

/** The maximum value of the range that defines the break.
 @since 100
 */
@property (nonatomic, assign, readwrite) double maxValue;

/** A symbol used to represent elements in the class break.
 @since 100
 */
@property (nullable, nonatomic, strong, readwrite) AGSSymbol * symbol;

#pragma mark -
#pragma mark methods

/** Compares this class break to another for equality.
 @param other class break to compare this one to
 @return whether equal or not
 @since 100
 */
-(BOOL)isEqualToClassBreak:(AGSClassBreak*)other;

NS_ASSUME_NONNULL_END
@end
