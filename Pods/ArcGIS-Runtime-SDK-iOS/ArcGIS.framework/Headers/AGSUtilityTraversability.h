/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTraversability.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityTraceCondition;
@class AGSUtilityTraceFunctionBarrier;

/** Used to specify whether traversability criteria are applied to junctions, edges, or both.
 @see @c AGSUtilityTraversability
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilityTraversabilityScope) {
    AGSUtilityTraversabilityScopeJunctionsAndEdges = 0,  /*!< The traversability criteria should be evaluated at both junctions and edges. */
    AGSUtilityTraversabilityScopeJunctions = 1,          /*!< The traversability criteria should only be evaluated at junctions. */
    AGSUtilityTraversabilityScopeEdges = 2               /*!< The traversability criteria should only be evaluated at edges. */
};

/** @brief A set of options controlling what objects are evaluated or returned
 during a tracing operation

 @since 100.7
 */
@interface AGSUtilityTraversability : AGSObject

#pragma mark -
#pragma mark initializers

/** Creates an @c AGSUtilityTraversability object with default values
 @since 100.7
 */
-(instancetype)init;

/** Creates an @c AGSUtilityTraversability object with default values
 @since 100.7
 */
+(instancetype)utilityTraversability;

#pragma mark -
#pragma mark properties

/** A condition object specifying when to traverse a node or its subnodes
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSUtilityTraceCondition *barriers;

/** A collection of @c AGSUtilityTraceFunctionBarrier objects. If any of these objects evaluates to true, further traversal is terminated.
 @since 100.7
 */
@property (nonatomic, copy, readwrite) NSArray<AGSUtilityTraceFunctionBarrier *> *functionBarriers;

/** Determines whether traversability criteria are evaluated on edges, junctions, or both.
 The default value is @c AGSUtilityTraversabilityScopeJunctionsAndEdges.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) AGSUtilityTraversabilityScope scope;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
