/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityPropagator.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityNetworkAttribute;

/** The function types that can be used with @c AGSUtilityPropagator objects
 @see @c AGSUtilityPropagator
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilityPropagatorFunctionType) {
    AGSUtilityPropagatorFunctionTypeBitwiseAnd = 1,  /*!< The network attribute value of the current element is bitwise-anded with the network attribute value propagated from the source. */
    AGSUtilityPropagatorFunctionTypeMax = 2,         /*!< The network attribute value of the current element is compared against the network attribute value propagated from the source -  the maximum of these values is propagated further. */
    AGSUtilityPropagatorFunctionTypeMin = 3          /*!< The network attribute value of the current element is compared against the network attribute value propagated from the source -  the minimum of these values is propagated further. */
};

/** @brief Propagator objects allow a subset of @c AGSUtilityNetworkAttribute values to propagate through a network while executing a trace.

 The propagated @c AGSUtilityNetworkAttribute values can be tested to allow or disallow further traversal.

 In the example of phase propagation, open devices along the network will restrict some phases from continuing along the trace. 

 Propagators only apply to subnetwork-based traces (@c AGSUtilityTraceTypeUpstream, @c AGSUtilityTraceTypeDownstream, and so on).
 @since 100.7
 */
@interface AGSUtilityPropagator : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates an object that allows a subset of @c AGSUtilityNetworkAttribute values to propagate through a network
 @param networkAttribute The @c AGSUtilityNetworkAttribute to propagate further along the trace
 @param propagatorFunctionType The function type that is applied to the @c AGSUtilityNetworkAttribute to propagate the attribute further along the trace
 @param comparisonOperator The filter operator that is applied when executing the trace. This operator is used to compare the propagated value to the specified @c AGSUtilityPropagator#value
 @param value The value that is compared against the propagated value when executing the trace
 @since 100.7
 */
-(instancetype)initWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute 
                 propagatorFunctionType:(AGSUtilityPropagatorFunctionType)propagatorFunctionType 
                     comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator 
                                  value:(id)value;

/** Creates an object that allows a subset of @c AGSUtilityNetworkAttribute values to propagate through a network
 @param networkAttribute The @c AGSUtilityNetworkAttribute to propagate further along the trace
 @param propagatorFunctionType The function type that is applied to the @c AGSUtilityNetworkAttribute to propagate the attribute further along the trace
 @param comparisonOperator The filter operator that is applied when executing the trace. This operator is used to compare the propagated value to the specified @c AGSUtilityPropagator#value
 @param value The value that is compared against the propagated value when executing the trace
 @since 100.7
 */
+(instancetype)utilityPropagatorWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute 
                              propagatorFunctionType:(AGSUtilityPropagatorFunctionType)propagatorFunctionType 
                                  comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator 
                                               value:(id)value;

/** Creates a object that allows a subset of @c AGSUtilityNetworkAttribute values to propagate through a network
 This constructor allows the specification of a substitution network attribute.
 @param networkAttribute The @c AGSUtilityNetworkAttribute to propagate further along the trace
 @param propagatorFunctionType The function type that is applied to the @c AGSUtilityNetworkAttribute to propagate the attribute further along the trace
 @param comparisonOperator The filter operator that is applied when executing the trace. This operator is used to compare the propagated value to the specified @c AGSUtilityPropagator#value
 @param value The value that is compared against the propagated value when executing the trace
 @param substitutionAttribute An @c AGSUtilityNetworkAttribute that maps each bit in another bitset network attribute value to a new value.
 @since 100.7
 */
-(instancetype)initWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute 
                 propagatorFunctionType:(AGSUtilityPropagatorFunctionType)propagatorFunctionType 
                     comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator 
                                  value:(id)value
                  substitutionAttribute:(AGSUtilityNetworkAttribute *)substitutionAttribute;

/** Creates a object that allows a subset of @c AGSUtilityNetworkAttribute values to propagate through a network
 This constructor allows the specification of a substitution network attribute.
 @param networkAttribute The @c AGSUtilityNetworkAttribute to propagate further along the trace
 @param propagatorFunctionType The function type that is applied to the @c AGSUtilityNetworkAttribute to propagate the attribute further along the trace
 @param comparisonOperator The filter operator that is applied when executing the trace. This operator is used to compare the propagated value to the specified @c AGSUtilityPropagator#value
 @param value The value that is compared against the propagated value when executing the trace
 @param substitutionAttribute An @c AGSUtilityNetworkAttribute that maps each bit in another bitset network attribute value to a new value.
 @since 100.7
 */
+(instancetype)utilityPropagatorWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute 
                              propagatorFunctionType:(AGSUtilityPropagatorFunctionType)propagatorFunctionType 
                                  comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator 
                                               value:(id)value
                               substitutionAttribute:(AGSUtilityNetworkAttribute *)substitutionAttribute;

#pragma mark -
#pragma mark properties

/** The operator that is applied when executing the trace.
 This operator is used to compare the propagated value to the specified @c AGSUtilityPropagator#value
 For example, if propagating phase, this would be @c AGSUtilityAttributeComparisonOperatorIncludesAny.
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityAttributeComparisonOperator comparisonOperator;

/** The @c AGSUtilityNetworkAttribute to propagate further along the trace
 For example, if propagating phase, this would be the @c AGSUtilityNetworkAttribute that stores phase.
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityNetworkAttribute *networkAttribute;

/** The function type that is applied to the @c AGSUtilityNetworkAttribute to propagate the attribute further along the trace
 For example, if propagating phase, this would be @c AGSUtilityPropagatorFunctionTypeBitwiseAnd.
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityPropagatorFunctionType propagatorFunctionType;

/** The @c AGSUtilityNetworkAttribute that maps each bit in another bitset network attribute value to a new value
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) AGSUtilityNetworkAttribute *substitutionNetworkAttribute;

/** The value that is compared against the propagated value when executing the trace
 @since 100.7
 */
@property (nonatomic, strong, readonly) id value;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
