/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityNetworkAttribute.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

@class AGSDomain;

NS_ASSUME_NONNULL_BEGIN

/** @brief A network attribute in a utility network
 
 A Network Attribute is an attribute that is copied and stored in the topological index.
 The utility network tracing task can read and make decisions using network attributes that are
 stored directly in the topological index. Without needing to fetch the individual features to get their attributes,
 the topological index provides performance that is orders of magnitude faster.
 
 The network attributes in a topological index are limited to specific data types and aggregate size.
 @since 100.6
 */
@interface AGSUtilityNetworkAttribute : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Whether the network attribute should be apportioned across the length of linear features.
 For example, if a point is chosen, 25% of the distance along an edge, 25% of the attribute's value is assigned to the 25% (shorter) part of the edge,
 and 75% of the attribute's value is assigned to the 75% (longer) part of the edge.
 @since 100.7
 */
@property (nonatomic, assign, readonly, getter=isApportionable) BOOL apportionable;

/** The @c AGSUtilityNetworkAttributeDataType for the @c AGSUtilityNetworkAttribute
 The network attributes in a topological index are limited to specific data types.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSUtilityNetworkAttributeDataType dataType;

/** The @c AGSDomain associated with this network attribute.
 If no domain is assigned, @c nil is returned.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) AGSDomain *domain;

/** The name of the @c AGSUtilityNetworkAttribute
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

/** The @c AGSUtilityNetworkAttribute that is substituted for this @c AGSUtilityNetworkAttribute at tap features
 This property is only set if @c AGSUtilityNetworkAttribute#substitution is set to @c YES.
 Taps are special features, identified with the "Subnetwork Tap" category, that work with attribute substitution. See the online help for more information.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) AGSUtilityNetworkAttribute *networkAttributeToSubstitute;

/** Whether the @c AGSUtilityNetworkAttribute is used as a substitution for another network attribute at tap features
 If this value is @c YES, this network attribute is the network attribute to substitute for @c AGSUtilityNetworkAttribute#networkAttributeToSubstitute at tap features.
 Taps are special features, identified with the "Subnetwork Tap" category, that work with attribute substitution.  See the online help for more information.
 @since 100.7
 */
@property (nonatomic, assign, readonly, getter=isSubstitution) BOOL substitution;

/** Indicates that the network attribute has been added by the system
 @since 100.7
 */
@property (nonatomic, assign, readonly, getter=isSystemDefined) BOOL systemDefined;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
