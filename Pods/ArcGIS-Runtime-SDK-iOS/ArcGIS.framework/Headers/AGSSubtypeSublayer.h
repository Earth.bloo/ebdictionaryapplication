/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSSubtypeSublayer.h */ //Required for Globals API doc

#import <ArcGIS/AGSArcGISSublayer.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSFeatureSubtype;
@class AGSLabelDefinition;
@class AGSRenderer;

/** @brief A sublayer that allows custom rendering for features of a particular subtype

 @since 100.7
 */
@interface AGSSubtypeSublayer : AGSArcGISSublayer

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The collection of label definitions for the sublayer
 @since 100.7
 */
@property (nonatomic, copy, readwrite) NSArray<AGSLabelDefinition *> *labelDefinitions;

/** The flag indicating whether the sublayer's labels are enabled/disabled.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL labelsEnabled;

/** The sublayer's maximum scale.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) double maxScale;

/** The sublayer's minimum scale.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) double minScale;

/** The sublayer's opacity.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) float opacity;

/** The sublayer's renderer.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSRenderer *renderer;

/** The subtype in the feature service that this subtype represents
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSFeatureSubtype *subtype;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
