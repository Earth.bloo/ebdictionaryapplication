/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSKMLColorStyle.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>
#import <ArcGIS/AGSColor.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Defines how an @c AGSKMLNode will appear 
 
 @c AGSKMLColorStyle defines how an @c AGSKMLNode will appear based on the specified #color and #colorMode properties. For @c AGSKMLIcon objects, the color that is specified is blended with the existing color of the base image.
 Due to this blending effect, to have the exact color appear that was specified by the color property, it is
 recommended that the base image be white. Also note that, because color defaults to white, the color property
 of @c AGSKMLIcon will result in the original color of the base image.
 @since 100.6
 */
@interface AGSKMLColorStyle : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The color.
 @since 100.6
 */
@property (nonatomic, strong, readwrite) AGSColor *color;

/** The color mode.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) AGSKMLColorMode colorMode;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
