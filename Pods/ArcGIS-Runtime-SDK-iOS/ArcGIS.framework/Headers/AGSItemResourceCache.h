/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSLoadableBase.h>

@class AGSLocalItem;

/** @file AGSItemResourceCache.h */ //Required for Globals API doc

/** @brief  Information on resources associated with an item.
 
 Instances of this class provide information on resources associated with an item. For instance, an item representing a vector tile layer has style resources such as fonts, sprites, etc.
 
 @since 100.2
 */
@interface AGSItemResourceCache : AGSLoadableBase

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize this object with a path to the resource cache on disk
 @param fileURL to a directory representing the resource cache on disk
 @return Initialized item resource cache
 @since 100.2
 */
-(instancetype)initWithFileURL:(NSURL *)fileURL;

/** Initialize this object with a path to the resource cache on disk
 @param fileURL to a directory representing the resource cache on disk
 @return Initialized item resource cache
 @since 100.2
 */
+(instancetype)itemResourceCacheWithFileURL:(NSURL*)fileURL;

#pragma mark -
#pragma mark properties

/** The path to the on-disk location containing the @c AGSItemResourceCache.
 The path can be a directory on-disk that contains the @c AGSItemResourceCache.
 Alternatively, if the @c AGSItemResourceCache was loaded from a package file, such as an @c AGSMobileMapPackage,
 the path is to the package.
 @since 100.2
 */
@property (nonatomic, strong, readonly) NSURL *fileURL;

/** The @c AGSLocalItem containing the @c AGSItemResourceCache's thumbnail and metadata.
This property can be @c nil if the @c AGSLocalItem is not present within the @c AGSItemResourceCache directory.
If the @c AGSItemResourceCache was taken offline using @c AGSExportVectorTilesTask or @c AGSOfflineMapTask
the @c AGSLocalItem will be populated from the online @c AGSPortalItem.
If the @c AGSItemResourceCache was created inside an @c AGSMobileMapPackage authored from ArcGIS Pro then
this property will return @c nil.
@since 100.2
 */
@property (nullable, nonatomic, strong, readonly) AGSLocalItem *item;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END

@end
