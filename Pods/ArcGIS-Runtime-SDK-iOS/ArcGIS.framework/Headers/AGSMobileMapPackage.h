/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSLoadableBase.h>

@class AGSLocatorTask;
@class AGSMap;
@class AGSItem;
@class AGSExpiration;

@protocol AGSCancelable;

NS_ASSUME_NONNULL_BEGIN

/** @file AGSMobileMapPackage.h */ //Required for Globals API doc

/** @brief A mobile map package
 
 Instances of this class represent a mobile map package (.mmpk file).
 
 A mobile map package can be created from ArcGIS Pro. It is a transport mechanism for mobile maps and scenes, their layers, and the layer's data. It contains metadata about the package (description, thumbnail, etc.), one or more mobile maps/scenes, layers, data and optionally networks and locators.
 
 @since 100
 @licenseExtn{StreetMap, when opening StreetMap Premium mobile map package}
 @ingroup licensing
 */
@interface AGSMobileMapPackage : AGSLoadableBase

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Initialize this object with the specified mobile map package (.mmpk file) on disk.
 @param fileURL to the mobile map package (.mmpk file) on disk.
 @return A new mobile map package object
 @since 100
 @licenseExtn{StreetMap, when opening StreetMap Premium mobile map package}
 @ingroup licensing
 */
-(instancetype)initWithFileURL:(NSURL *)fileURL;

/** Initialize this object with the name of a mobile map package (.mmpk file), excluding the ".mmpk" extension,
 within the application bundle or shared documents directory.
 @param name of the mobile map package (excluding the .mmpk extension)
 @return A new mobile map package object
 @since 100
 @licenseExtn{StreetMap, when opening StreetMap Premium mobile map package}
 @ingroup licensing
 */
-(instancetype)initWithName:(NSString *)name;

/** Initialize this object with the specified mobile map package (.mmpk file) on disk.
 @param fileURL to the mobile map package (.mmpk file) on disk.
 @return A new mobile map package object
 @since 100
 @licenseExtn{StreetMap, when opening StreetMap Premium mobile map package}
 @ingroup licensing
 */
+(instancetype)mobileMapPackageWithFileURL:(NSURL *)fileURL;

/** Initialize this object with the name of a mobile map package (.mmpk file), excluding the ".mmpk" extension,
 within the application bundle or shared documents directory.
 @param name of the mobile map package (excluding the .mmpk extension)
 @return A new mobile map package object
 @since 100
 @licenseExtn{StreetMap, when opening StreetMap Premium mobile map package}
 @ingroup licensing
 */
+(instancetype)mobileMapPackageWithName:(NSString *)name;

#pragma mark -
#pragma mark properties

/** Metadata associated with the mobile map package
 @since 100
 */
@property (nullable, nonatomic, strong, readonly) AGSItem *item;

/** The locator contained in the mobile map package
 @since 100
 */
@property (nullable, nonatomic, strong, readonly) AGSLocatorTask *locatorTask;

/** The maps contained in the mobile map package
 @since 100
 */
@property (nonatomic, copy, readonly) NSArray<AGSMap*> *maps;

/** The URL of the mobile map package (.mmpk file) on disk.
 @since 100
 */
@property (nonatomic, strong, readonly) NSURL *fileURL;

/** Version of the mobile map package
 @since 100
 */
@property (nonatomic, copy, readonly) NSString *version;

/** Expiration details for this mobile map package, if provided. If the package has expired and was authored as @c ExpirationTypePreventExpiredAccess, then it will fail to load and can no longer be used.
 @since 100.5
 */
@property (nullable, nonatomic, strong, readonly) AGSExpiration * expiration;

#pragma mark -
#pragma mark methods

/** Closes a mobile map package.
 Closes a mobile map package and frees file locks on the underlying .mmpk file or directory.
 
 All references to mobile map package data (maps, layers, tables, networks, locators, etc.)
 should be released before closing the package. If active references to mobile map package
 data exist, this method will still close the package, but subsequent rendering and data
 access methods will fail. Results of accessing mobile map package data after
 closing it are undefined.
 
 After closing a mobile map package, the underlying .mmpk file or directory can be moved or deleted.
 
 Closing a mobile map package is not necessary if the package has not been loaded.
 @see @c AGSMobileScenePackage#close, @c AGSGeodatabase#close
 @since 100.6
 */
-(void)close;

/** Unpacks a mobile map package file (.mmpk) to the specified directory.
 Note that unpacking will fail if the package is expired and was authored as @c AGSExpirationTypePreventExpiredAccess.
 @param fileURL location to mmpk file
 @param outputDirectory specifying where to unpack the package. If the last component of the directory location does not exist, it will be created during unpacking.
 @param completion block that is invoked when the operation completes.
 @return operation that can be canceled
 @since 100.2.1
 */
+(id<AGSCancelable>)unpackMobileMapPackageAtFileURL:(NSURL *)fileURL
                                    outputDirectory:(NSURL*)outputDirectory
                                         completion:(void(^)(NSError * __nullable error))completion;

@end

@interface AGSMobileMapPackage (AGSDeprecated)

/** Checks if the package can be read directly without requiring to be unpacked. Always returns true after deprecation in version 100.7
 - Prior to version 100.7, some data formats could only be accessed if they were present on disk, for example, @c AGSRasterLayer. In these situations, this method would return false and you would need to unpack the package to access the data.
 - From version 100.7 and onwards this limitation has been removed allowing the data to be read directly from the mobile map package. This method always returns a result of true.
 Since this method is no longer required it can be removed from calling code including any subsequent use of @c AGSMobileMapPackage#unpackWithMobileMapPackageFileURL:outputDirectory:completion:.
 @param fileURL location to mmpk file
 @param completion block that is invoked with information about whether direct read is supported if the operation succeeds, or an error if it fails
 @return operation that can be canceled
 @see @c AGSMobileMapPackage#unpackWithMobileMapPackageFileURL:outputDirectory:completion:
 @since 100.2.1
 @deprecated 100.7 This method is no longer required as the result is always true. It can be removed from calling code including any subsequent use of @c AGSMobileMapPackage#unpackWithMobileMapPackageFileURL:outputDirectory:completion:.
 */
+(id<AGSCancelable>)checkDirectReadSupportForMobileMapPackageAtFileURL:(NSURL*)fileURL
                                                            completion:(void(^)(BOOL isDirectReadSupported, NSError * __nullable error))completion __deprecated_msg("This method is no longer required as the result is always true. It can be removed from calling code including any subsequent use of 'unpackWithMobileMapPackageFileURL:outputDirectory:completion:'.");

@end

NS_ASSUME_NONNULL_END
