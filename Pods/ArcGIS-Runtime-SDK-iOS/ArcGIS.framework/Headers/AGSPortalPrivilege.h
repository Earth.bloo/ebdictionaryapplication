/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSPortalPrivilege.h */ //Required for Globals API doc

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/** Supported portal privilege realms.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalPrivilegeRealm) {
    AGSPortalPrivilegeRealmUnknown = 0,
    AGSPortalPrivilegeRealmFeatures,
    AGSPortalPrivilegeRealmMarketplace,
    AGSPortalPrivilegeRealmOpenData,
    AGSPortalPrivilegeRealmPortal,
    AGSPortalPrivilegeRealmPremium,
};

/** Supported portal privilege roles.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalPrivilegeRole) {
    AGSPortalPrivilegeRoleUnknown = 0,
    AGSPortalPrivilegeRoleUser,
    AGSPortalPrivilegeRoleAdmin,
    AGSPortalPrivilegeRolePublisher,
};

/** Supported portal privilege types.
 @since 100.0
 */
typedef NS_ENUM(NSInteger, AGSPortalPrivilegeType) {
    AGSPortalPrivilegeTypeUnknown = 0,               /*!< An unknown portal privilege type. */
    AGSPortalPrivilegeTypeEdit,                      /*!< Grants the ability to edit features in editable layers, according to the edit options enabled on the layer. */
    AGSPortalPrivilegeTypeFullEdit,                  /*!< Grants the ability to add, delete, and update features in a hosted feature layer regardless of the editing options enabled on the layer. */
    AGSPortalPrivilegeTypeManage,                    /*!< Grants the ability to create listings, list items and manage subscriptions in ArcGIS Marketplace. */
    AGSPortalPrivilegeTypePurchase,                  /*!< Grants the ability to request purchase information about apps and data in ArcGIS Marketplace. */
    AGSPortalPrivilegeTypeStartTrial,                /*!< Grants the ability to start trial subscriptions in ArcGIS Marketplace. */
    AGSPortalPrivilegeTypeDesignateGroup,            /*!< Grants the ability to designate groups within the organization as being available for use in Open Data. */
    AGSPortalPrivilegeTypeOpenDataAdmin,             /*!< Grants the ability to manage Open Data Sites for the organization. */
    AGSPortalPrivilegeTypeAssignToGroups,            /*!< Grants the ability to assign members to, and remove members from, groups within the organization. */
    AGSPortalPrivilegeTypeChangeUserRoles,           /*!< Grants the ability to change the role a member is assigned within the organization. */
    AGSPortalPrivilegeTypeDeleteGroups,              /*!< Grants the ability to delete groups within the organization. */
    AGSPortalPrivilegeTypeDeleteItems,               /*!< Grants the ability to delete content within the organization. */
    AGSPortalPrivilegeTypeDeleteUsers,               /*!< Grants the ability to delete member accounts within the organization. */
    AGSPortalPrivilegeTypeDisableUsers,              /*!< Grants the ability to enable and disable member accounts within the organization. */
    AGSPortalPrivilegeTypeInviteUsers,               /*!< Grants the ability to invite members to the organization. */
    AGSPortalPrivilegeTypeManageEnterpriseGroups,    /*!< Grants the ability to link group membership to an enterprise group. */
    AGSPortalPrivilegeTypeManageLicenses,            /*!< Grants the ability to assign licenses to members of the organization. */
    AGSPortalPrivilegeTypeReassignGroups,            /*!< Grants the ability to reassign groups to other members within the organization. */
    AGSPortalPrivilegeTypeReassignItems,             /*!< Grants the ability to reassign content to other members within the organization. */
    AGSPortalPrivilegeTypeReassignUsers,             /*!< Grants the ability to assign all groups and content of a member to another within the organization. */
    AGSPortalPrivilegeTypeUpdateGroups,              /*!< Grants the ability to update groups within the organization. */
    AGSPortalPrivilegeTypeUpdateItems,               /*!< Grants the ability to update and categorize content within the organization. */
    AGSPortalPrivilegeTypeUpdateUsers,               /*!< Grants the ability to update member account information within the organization. */
    AGSPortalPrivilegeTypeViewGroups,                /*!< Grants the ability to view all groups within the organization. */
    AGSPortalPrivilegeTypeViewItems,                 /*!< Grants the ability to view all content within the organization. */
    AGSPortalPrivilegeTypeViewUsers,                 /*!< Grants the ability to view full member account information within the organization. */
    AGSPortalPrivilegeTypePublishFeatures,           /*!< Grants the ability to publish hosted feature layers from shapefiles, CSVs, etc. */
    AGSPortalPrivilegeTypePublishScenes,             /*!< Grants the ability to publish hosted scene layers. */
    AGSPortalPrivilegeTypePublishTiles,              /*!< Grants the ability to publish hosted tile layers from tile packages, features, etc. */
    AGSPortalPrivilegeTypeCreateGroup,               /*!< Grants the ability for a member to create, edit, and delete their own groups. */
    AGSPortalPrivilegeTypeCreateItem,                /*!< Grants the ability for a member to create, edit, and delete their own content. */
    AGSPortalPrivilegeTypeJoinGroup,                 /*!< Grants the ability to join groups within the organization. */
    AGSPortalPrivilegeTypeJoinNonOrgGroup,           /*!< Grants the ability to join groups external to the organization. */
    AGSPortalPrivilegeTypeShareGroupToOrg,           /*!< Grants the ability to make groups discoverable by the organization. */
    AGSPortalPrivilegeTypeShareGroupToPublic,        /*!< Grants the ability to make groups discoverable by all users of the portal. */
    AGSPortalPrivilegeTypeShareToGroup,              /*!< For a user, grants the ability to share content to groups.
                                                      For an administrator, grants the ability to share other member's content to groups the user belongs to.
                                                      */
    AGSPortalPrivilegeTypeShareToOrg,                /*!< For a user, grants the ability to share content to the organization.
                                                      For an administrator, grants the ability to share other member's content to the organization.
                                                      */
    AGSPortalPrivilegeTypeShareToPublic,             /*!< For a user, grants the ability to share content to all users of the portal.
                                                      For an administrator, grants the ability to share other member's content to all users of the portal.
                                                      */
    AGSPortalPrivilegeTypeDemographics,              /*!< Grants the ability to make use of premium demographic data. */
    AGSPortalPrivilegeTypeElevation,                 /*!< Grants the ability to perform analytical tasks on elevation data. */
    AGSPortalPrivilegeTypeGeocode,                   /*!< Grants the ability to perform large-volume geocoding tasks with the Esri World Geocoder such as publishing a CSV of addresses as a hosted feature layer. */
    AGSPortalPrivilegeTypeGeoEnrichment,             /*!< Grants the ability to geoenrich features. */
    AGSPortalPrivilegeTypeNetworkAnalysis,           /*!< Grants the ability to perform network analysis tasks such as routing and drive-time areas. */
    AGSPortalPrivilegeTypeSpatialAnalysis,           /*!< Grants the ability to perform spatial analysis tasks. */
    AGSPortalPrivilegeTypeCreateUpdateCapableGroup,  /*!< Grants the ability to create and own groups with item update capabilities. */
    AGSPortalPrivilegeTypeViewOrgGroups,             /*!< Grants the ability to view groups shared with the organization. */
    AGSPortalPrivilegeTypeViewOrgItems,              /*!< Grants the ability to view content shared with the organization. */
    AGSPortalPrivilegeTypeViewOrgUsers,              /*!< Grants the ability to view members of the organization. */
    AGSPortalPrivilegeTypeGeoAnalytics,              /*!< Grants the ability to use big data geoanalytics. */
    AGSPortalPrivilegeTypeRasterAnalysis,            /*!< Grants the ability to use raster analysis. */
    AGSPortalPrivilegeTypePublishServerGPServices,   /*!< Grants the ability to publish non-hosted server geoprocessing services. */
    AGSPortalPrivilegeTypePublishServerServices,     /*!< Grants the ability to publish non-hosted server services. */
    AGSPortalPrivilegeTypeUpdateItemCategorySchema,  /*!< Grants the ability to configure the organization content category schema. */
    AGSPortalPrivilegeTypeFeatureReport,             /*!< Grants the ability to generate feature reports. */
    AGSPortalPrivilegeTypeManageCollaborations,       /*!< Grants the ability to manage the organization's collaborations. */
    AGSPortalPrivilegeTypeManageCredits,             /*!< Grants the ability to manage the organization's credit settings. */
    AGSPortalPrivilegeTypeManageRoles,               /*!< Grants the ability to manage the organization's member roles. */
    AGSPortalPrivilegeTypeManageSecurity,            /*!< Grants the ability to manage the organization's security settings. */
    AGSPortalPrivilegeTypeManageServers,             /*!< Grants the ability to manage the portal's server settings. */
    AGSPortalPrivilegeTypeManageUtilityServices,     /*!< Grants the ability to manage the organization's utility service settings. */
    AGSPortalPrivilegeTypeManageWebsite,              /*!< Grants the ability to manage the organization's website settings. */
    AGSPortalPrivilegeTypeManageReplications,         /*!< Grants the ability to manage replications and utilize the collaborations API. */
    AGSPortalPrivilegeTypeCreateNotebooks,            /*!< Grants the ability to create and edit interactive notebook documents. */
    AGSPortalPrivilegeTypeCreateAdvancedNotebooks,    /*!< Grants the ability to publish a notebook as a geoprocessing service. */
    AGSPortalPrivilegeTypeBulkPublishFromDataStores,  /*!< Grants the ability to publish web layers from a registered data store. */
    AGSPortalPrivilegeTypeEnumerateDataStores,        /*!< Grants the ability to get the list of datasets from a registered data store. */
    AGSPortalPrivilegeTypeRegisterDataStores,         /*!< Grants the ability to register data stores to the portal. */
    AGSPortalPrivilegeTypeCategorizeItems,            /*!< Grants the ability to categorize items in groups. */
    AGSPortalPrivilegeTypeViewTracks                  /*!< Grants the ability to view members' location tracks via shared track views when location tracking is enabled. */
};

/** @brief Privileges of a portal user
 
 Instances of this class represent a previlege possessed by a portal user. This privlege permits the user to peform specific operations on the portal.
 
 @since 100
 */
@interface AGSPortalPrivilege : NSObject

/** The realm the privilege belongs to. Helps to organize privileges into categories for aspects relating to a portal.
 @since 100
 */
@property (nonatomic, assign, readonly) AGSPortalPrivilegeRealm realm;

/** The role the privilege applies to. 
 @since 100
 */
@property (nonatomic, assign, readonly) AGSPortalPrivilegeRole role;

/** The operation permitted by the privilege.
 @since 100
 */
@property (nonatomic, assign, readonly) AGSPortalPrivilegeType type;

/** The type represented as a string.
 @since 100
 */
@property (nonatomic, copy, readonly) NSString *typeName;

@end

NS_ASSUME_NONNULL_END
