/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSFillSymbol.h>
#import <ArcGIS/AGSLoadable.h>
#import <ArcGIS/AGSRemoteResource.h>
#import <ArcGIS/AGSImage.h>

/** @file AGSPictureFillSymbol.h */ //Required for Globals API doc

/** @brief Used to draw polygon features on a layer using pictures.
 
 Picture fill symbols display fills using a picture image.
 
 @since 100
 */
@interface AGSPictureFillSymbol : AGSFillSymbol <AGSLoadable, AGSRemoteResource>

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

/** Initialize a picture fill symbol with the provided image
 @param image to use with the symbol
 @return Initialized picture fill symbol
 @since 100
 */
-(instancetype)initWithImage:(AGSImage*)image;

/** Initialize a picture fill symbol with the provided URL to an image
 @param URL to an image
 @return Initialized picture fill symbol
 @since 100
 */
-(instancetype)initWithURL:(NSURL*)URL;

/** Initialize a picture fill symbol with the provided image
 @param image to use with the symbol
 @return Initialized picture fill symbol
 @since 100
 */
+(instancetype)pictureFillSymbolWithImage:(AGSImage*)image;

/** Initialize a picture fill symbol with the provided URL to an image
 @param URL to an image
 @return Initialized picture fill symbol
 @since 100
 */
+(instancetype)pictureFillSymbolWithURL:(NSURL*)URL;

#pragma mark -
#pragma mark properties

/** The image being used by the picture fill symbol. May be nil until `#loadStatus` is @c AGSLoadStatusLoaded
 @since 100
 */
@property (nullable, nonatomic, strong, readonly) AGSImage *image;

/** The height (in points, not pixels) of the symbol. Defaults to size of the image.
 @since 100
 */
@property (nonatomic, assign, readwrite) CGFloat height;

/** The width (in points, not pixels) of the symbol. Defaults to size of the image.
 @since 100
 */
@property (nonatomic, assign, readwrite) CGFloat width;

/** Opacity of the symbol. Defaults to 1 (fully opaque)
 Permitted values range between 0-1 (both inclusive)
 @since 100
 */
@property (nonatomic, assign, readwrite) float opacity;

/** Angle of the symbol.
 @since 100.1
 */
@property (nonatomic, assign, readwrite) double angle;

/** The X scale of the symbol.
@since 100.1
*/
@property (nonatomic, assign, readwrite) double scaleX;

/** The Y scale of the symbol.
@since 100.1
*/
@property (nonatomic, assign, readwrite) double scaleY;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END

@end
