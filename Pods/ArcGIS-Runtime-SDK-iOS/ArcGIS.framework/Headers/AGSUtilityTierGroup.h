/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTierGroup.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityTier;

/** @brief Tier groups provide an extra level of organization for tiers.

 For example, a gas network may be divided into two tier groups - Transmission and Distribution. 
 Each of these tier groups would contain a set of tiers specific to that group. 
 For example, Distribution Pressure and Distribution Isolation might be tiers within the Distribution tier group.
 @since 100.7
 */
@interface AGSUtilityTierGroup : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The name of the @c AGSUtilityTierGroup
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSString *name;

/** The collection of @c AGSUtilityTier objects within this tier group.
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityTier *> *tiers;

#pragma mark -
#pragma mark methods

/** Gets the @c AGSUtilityTier object with the specified name
 @param name The name of the desired @c AGSUtilityTier object to return.
 @return An @c AGSUtilityTier object with the specified name.
 @since 100.7
 */
-(nullable AGSUtilityTier *)tierWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
