/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSImage.h */ //Required for Globals API doc

#if __has_include(<UIKit/UIImage.h>)
#import <UIKit/UIImage.h>
/**
 @c AGSImage is defined as @c UIImage for iOS platform.
 
 @since 100
 */
#define AGSImage UIImage
#elif __has_include(<AppKit/NSImage.h>)
#import <AppKit/NSImage.h>
/**
 @c AGSImage is defined as @c NSImage for macOS platforms.
 
 @since 100
 */
#define AGSImage NSImage
#endif
