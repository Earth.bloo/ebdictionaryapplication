/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTraceConditionalExpression.h */ //Required for Globals API doc

#import <ArcGIS/AGSUtilityTraceCondition.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief A trace condition that depends on the schema information in the Utility
 Network, such as the existence of an @c AGSUtilityCategory on a node, or
 the value of an @c AGSUtilityNetworkAttribute

 @since 100.7
 */
@interface AGSUtilityTraceConditionalExpression : AGSUtilityTraceCondition

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
