/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSPortalGroupContentSearchParameters.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSEnvelope;

/** @brief Contains search parameters suitable for finding portal items that belong to a portal group

 You can create an instance using one of the factory methods that are designed for particular types of searches,
 or you can use one of the constructors and specify a custom query string. A number of properties are also
 available to customize various aspects of the search.

 A typical usage pattern is:
 - Create a new PortalGroupContentSearchParameters object using a factory method or constructor.
 - Optionally set one or more of the properties.
 - Use the object by passing it to @c AGSPortalGroup#findItemsWithSearchParameters:completion:.
 - Optionally call @c AGSPortalGroupContentSearchResultSet#nextSearchParameters to get a new 
 PortalGroupContentSearchParameters object that can be used to 'find' the next batch of results.
 @see @c AGSPortalGroup#findItemsWithSearchParameters:completion:
 @since 100.7
 */
@interface AGSPortalGroupContentSearchParameters : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Constructor that sets a bounding box for a spatial search
 @param boundingBox an Envelope specifying the bounding box
 @since 100.7
 */
-(instancetype)initWithBoundingBox:(AGSEnvelope *)boundingBox;

/** Constructor that sets a bounding box for a spatial search
 @param boundingBox an Envelope specifying the bounding box
 @since 100.7
 */
+(instancetype)portalGroupContentSearchParametersWithBoundingBox:(AGSEnvelope *)boundingBox;

/** Constructor that sets a custom query string to use
 @param query the query string to use, for example "owner:username AND title:california"
 @since 100.7
 */
-(instancetype)initWithQuery:(NSString *)query;

/** Constructor that sets a custom query string to use
 @param query the query string to use, for example "owner:username AND title:california"
 @since 100.7
 */
+(instancetype)portalGroupContentSearchParametersWithQuery:(NSString *)query;

/** Constructor that sets a custom query string to use and a bounding box for a spatial search
 @param query the query string to use, for example "owner:username AND title:california"
 @param boundingBox an Envelope specifying the bounding box
 @since 100.7
 */
-(instancetype)initWithQuery:(NSString *)query 
                 boundingBox:(nullable AGSEnvelope *)boundingBox;

/** Constructor that sets a custom query string to use and a bounding box for a spatial search
 @param query the query string to use, for example "owner:username AND title:california"
 @param boundingBox an Envelope specifying the bounding box
 @since 100.7
 */
+(instancetype)portalGroupContentSearchParametersWithQuery:(NSString *)query 
                                               boundingBox:(nullable AGSEnvelope *)boundingBox;

/** Constructor that sets a custom query string to use and a limit on the number of results to return
 @param query the query string to use, for example "owner:username AND title:california"
 @param limit the maximum number of results to be returned
 @since 100.7
 */
-(instancetype)initWithQuery:(NSString *)query 
                       limit:(NSInteger)limit;

/** Constructor that sets a custom query string to use and a limit on the number of results to return
 @param query the query string to use, for example "owner:username AND title:california"
 @param limit the maximum number of results to be returned
 @since 100.7
 */
+(instancetype)portalGroupContentSearchParametersWithQuery:(NSString *)query 
                                                     limit:(NSInteger)limit;

#pragma mark -
#pragma mark properties

/** An Envelope specifying the bounding box for a spatial search
 Spatial search is an overlaps/intersects function of the bounding box and the extent of the document.
 Documents that have no extent (e.g., mxds, 3dds, lyr) will not be found when doing a bounding box search.
 Document extent is assumed to be in the WGS84 geographic coordinate system.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSEnvelope *boundingBox;

/** An Array of content category specifications to use when searching for items
 Each entry in the Array is a String containing a comma-separated list of up to 8 group content categories. 
 The full path of each category is required and an OR relationship is applied between the categories
 within a particular String.

 There can be up to 8 Strings with an AND relationship being applied between the different Strings.

 For example, to search for items belonging to either the Water or Forest categories, both within the US,
 specify 2 Strings as follows: "/Categories/Water,/Categories/Forest" and "/Region/US".
 @since 100.7
 */
@property (nonatomic, copy, readwrite) NSArray<NSString *> *categories;

/** The maximum number of results to be included in the result set response
 The limit, along with the @c AGSPortalGroupContentSearchParameters#startIndex, can be used to paginate the 
 search results.

 The default value is 10, and the maximum allowed value is 100.

 Note that the actual number of returned results may be less than limit. This happens when the number of 
 results remaining after the startIndex is less than limit.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) NSInteger limit;

/** The query string specified for the search.
 @see http://resources.arcgis.com/en/help/arcgis-rest-api/index.html#//02r3000000mn000000 for information on query syntax 
 @since 100.7
 */
@property (nullable, nonatomic, copy, readonly) NSString *query;

/** Indicates whether public items outside the organization may be included in the search results
 Default behavior is they are included.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL searchPublic;

/** A string containing one or more fields names, comma separated, specifying the field(s) to sort the results by
 Sort field names are case-insensitive. Supported field names are "title", "created", "type", "owner", "modified", 
 "added", "avgrating", "numratings", "numcomments", and "numviews".
 @since 100.7
 */
@property (nullable, nonatomic, copy, readwrite) NSString *sortField;

/** The sort order, indicating whether the results are returned in ascending or descending order
 Default behavior is ascending order.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) AGSPortalQuerySortOrder sortOrder;

/** The index within the entire set of results of the first entry in the current page
 The startIndex, along with the @c AGSPortalGroupContentSearchParameters#limit, can be used to paginate the 
 search results. The index number is 1-based and the default value is 1.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) NSInteger startIndex;

#pragma mark -
#pragma mark methods

/** Creates an @c AGSPortalGroupContentSearchParameters that will find items with a particular type
 Optionally restricts the search to items belonging to a specified owner. An optional search string can be 
 used to restrict the search even further.
 @param type the item type to search for
 @param owner the username of the owner of the items, or null to search for items with any owner
 @param searchString a string specifying other criteria for the search, or null if there are none
 @since 100.7
 */
+(AGSPortalGroupContentSearchParameters *)portalGroupContentSearchParametersForItemsOfType:(AGSPortalItemType)type 
                                                                                     owner:(nullable NSString *)owner 
                                                                              searchString:(nullable NSString *)searchString;

/** Creates an @c AGSPortalGroupContentSearchParameters that will find items with a specified type and, optionally,
 that also match a specified search string.
 @param type item type to search for
 @param searchString a string specifying other criteria for the search, or null if there are none
 @since 100.7
 */
+(AGSPortalGroupContentSearchParameters *)portalGroupContentSearchParametersForItemsOfType:(AGSPortalItemType)type 
                                                                              searchString:(nullable NSString *)searchString;

/** Creates an @c AGSPortalGroupContentSearchParameters that will find items with any one of a number of given types
 Optionally restricts the search to items belonging to a specified owner. An optional search string can be 
 used to restrict the search even further.
 @param types the item types to search for
 @param owner the username of the owner of the items, or null to search for items with any owner
 @param searchString a string specifying other criteria for the search, or null if there are none
 @since 100.7
 */
+(AGSPortalGroupContentSearchParameters *)portalGroupContentSearchParametersForItemsOfTypes:(NSArray<NSValue *> *)types 
                                                                                      owner:(nullable NSString *)owner 
                                                                               searchString:(nullable NSString *)searchString;

/** Creates an @c AGSPortalGroupContentSearchParameters that will find items belonging to a specified owner
 @param owner the username of the owner of the items
 @since 100.7
 */
+(AGSPortalGroupContentSearchParameters *)portalGroupContentSearchParametersForItemsWithOwner:(NSString *)owner;

/** Creates an @c AGSPortalGroupContentSearchParameters that will find an item with a specified item ID
 @param itemID the ID of the item
 @since 100.7
 */
+(AGSPortalGroupContentSearchParameters *)portalGroupContentSearchParametersForItemWithID:(NSString *)itemID;

@end

NS_ASSUME_NONNULL_END
