/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityNetworkAttributeComparison.h */ //Required for Globals API doc

#import <ArcGIS/AGSUtilityTraceConditionalExpression.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityNetworkAttribute;

/** @brief A condition evaluating the value of an @c AGSUtilityNetworkAttribute on
 nodes in the network, either to another @c AGSUtilityNetworkAttribute or
 to a specific value

 @since 100.7
 */
@interface AGSUtilityNetworkAttributeComparison : AGSUtilityTraceConditionalExpression

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a condition comparing the value of one 
 @c AGSUtilityNetworkAttribute to another
 @param networkAttribute The source @c AGSUtilityNetworkAttribute that the value is compared to
 @param comparisonOperator The type of comparison to do between the source @c AGSUtilityNetworkAttribute and another @c AGSUtilityNetworkAttribute
 @param otherNetworkAttribute The @c AGSUtilityNetworkAttribute being compared against another @c AGSUtilityNetworkAttribute on each of the nodes
 @since 100.7
 */
-(nullable instancetype)initWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute
                              comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator
                           otherNetworkAttribute:(AGSUtilityNetworkAttribute *)otherNetworkAttribute;

/** Creates a condition comparing the value of one 
 @c AGSUtilityNetworkAttribute to another
 @param networkAttribute The source @c AGSUtilityNetworkAttribute that the value is compared to
 @param comparisonOperator The type of comparison to do between the source @c AGSUtilityNetworkAttribute and another @c AGSUtilityNetworkAttribute
 @param otherNetworkAttribute The @c AGSUtilityNetworkAttribute being compared against another @c AGSUtilityNetworkAttribute on each of the nodes
 @since 100.7
 */
+(nullable instancetype)utilityNetworkAttributeComparisonWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute
                                                           comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator
                                                        otherNetworkAttribute:(AGSUtilityNetworkAttribute *)otherNetworkAttribute;

/** Creates a condition comparing an @c AGSUtilityNetworkAttribute to a
 specific value
 @param networkAttribute The source @c AGSUtilityNetworkAttribute that the value is compared to
 @param comparisonOperator The type of comparison to do between the source @c AGSUtilityNetworkAttribute and the value
 @param value The specific value to compare networkAttribute against for each node in the trace
 @since 100.7
 */
-(nullable instancetype)initWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute
                              comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator
                                           value:(id)value;

/** Creates a condition comparing an @c AGSUtilityNetworkAttribute to a
 specific value
 @param networkAttribute The source @c AGSUtilityNetworkAttribute that the value is compared to
 @param comparisonOperator The type of comparison to do between the source @c AGSUtilityNetworkAttribute and the value
 @param value The specific value to compare networkAttribute against for each node in the trace
 @since 100.7
 */
+(nullable instancetype)utilityNetworkAttributeComparisonWithNetworkAttribute:(AGSUtilityNetworkAttribute *)networkAttribute
                                                           comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator 
                                                                        value:(id)value;

#pragma mark -
#pragma mark properties

/** The type of comparison to do between the source
 @c AGSUtilityNetworkAttribute and either the second
 @c AGSUtilityNetworkAttribute or the value
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityAttributeComparisonOperator comparisonOperator;

/** The source @c AGSUtilityNetworkAttribute that otherNetworkAttribute or value is compared
 to
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityNetworkAttribute *networkAttribute;

/** The @c AGSUtilityNetworkAttribute being compared against @c AGSUtilityNetworkAttributeComparison#networkAttribute on each of the
 nodes
 This is @c nil if the comparison is against a specific value.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) AGSUtilityNetworkAttribute *otherNetworkAttribute;

/** The specific value to compare networkAttribute against for each node
 in the trace
 @since 100.7
 */
@property (nullable, nonatomic, strong, readonly) id value;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
