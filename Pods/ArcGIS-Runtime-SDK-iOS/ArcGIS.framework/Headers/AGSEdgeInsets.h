/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSDefines.h>

#if __has_include(<UIKit/UIGeometry.h>)

#import <UIKit/UIGeometry.h>

/**
 @c AGSEdgeInsets is defined as @c UIEdgeInsets on iOS platform.
 
 Edge inset values are applied to a rectangle to shrink or expand the area
 represented by that rectangle. Positive values cause the object to be inset (or
 shrunk) by the specified amount. Negative values cause the object to be outset
 (or expanded) by the specified amount.
 
 @since 100.1
 */
typedef UIEdgeInsets AGSEdgeInsets;

/**
 Returns a @c CGRect that is inset from the original rect by the specified
 amount.
 
 @param rect The rect to inset.
 @param inset The amount by which to inset.
 @since 100.1
 */
static inline CGRect AGSEdgeInsetsInsetRect(CGRect rect, AGSEdgeInsets insets) {
    return UIEdgeInsetsInsetRect(rect, insets);
}

#elif __has_include(<Foundation/NSGeometry.h>)

#import <Foundation/NSGeometry.h>

/**
 @c AGSEdgeInsets is defined as @c NSEdgeInsets on macOS platform.
 
 Edge inset values are applied to a rectangle to shrink or expand the area
 represented by that rectangle. Positive values cause the object to be inset (or
 shrunk) by the specified amount. Negative values cause the object to be outset
 (or expanded) by the specified amount.
 
 @since 100.1
 */
typedef NSEdgeInsets AGSEdgeInsets;

/**
 Returns a @c CGRect that is inset from the original rect by the specified
 amount.
 
 @param rect The rect to inset.
 @param inset The amount by which to inset.
 @since 100.1
 */
static inline CGRect AGSEdgeInsetsInsetRect(CGRect rect, AGSEdgeInsets insets) {
    return CGRectMake(rect.origin.x + insets.left,
                      rect.origin.y + insets.bottom,
                      rect.size.width - (insets.right + insets.left),
                      rect.size.height - (insets.top + insets.bottom));
}

#endif

/**
 Edge inset values are applied to a rectangle to shrink or expand the area
 represented by that rectangle. Positive values cause the object to be inset (or
 shrunk) by the specified amount. Negative values cause the object to be outset
 (or expanded) by the specified amount.

 @param top The inset at the top edge.
 @param left The inset at the left edge.
 @param bottom The inset at the bottom edge.
 @param right The inset at the right edge.
 @return An edge insets.
 @since 100.1
 */
static inline AGSEdgeInsets AGSEdgeInsetsMake(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right) {
    AGSEdgeInsets insets = {top, left, bottom, right};
    return insets;
}

/**
 An @c AGSEdgeInsets struct whose top, left, bottom, and right fields are all
 set to the value 0.
 
 @since 100.1
 */
AGS_EXTERN const AGSEdgeInsets AGSEdgeInsetsZero;
