/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityAssetType.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityCategory;
@class AGSUtilityTerminalConfiguration;

/** @brief A utility network asset type
 
 @c AGSUtilityAssetType is the second-level categorization of an @c AGSUtilityNetworkSource. (@c AGSUtilityAssetGroup is the first-level categorization.)
 
 Examples of utility network asset types include
 * The @c AGSUtilityAssetType for a transformer @c AGSUtilityAssetGroup in an electric distribution domain network could be StepTransformer, PowerTransformer, or DistributionTransformer.
 * The @c AGSUtilityAssetType for a line feature class in a water distribution domain network could be PVCPipe, ClayPipe, or CastIronPipe.
 @since 100.6
 */
@interface AGSUtilityAssetType : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The @c AGSUtilityAssociationRole of the @c AGSUtilityAssetType
 This property indicates whether the @c AGSUtilityAssetType can be a
 container, a structure, or neither.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSUtilityAssociationRole associationRole;

/** The @c AGSUtilityCategory collection for the @c AGSUtilityAssetType
 This is a collection of system-provided network categories that
 incorporate semantics of the utility network for subnetwork
 management and tracing operations.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityCategory *> *categories;

/** The code of the @c AGSUtilityAssetType
 This property is the attribute domain value.
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger code;

/** The container view scale of the @c AGSUtilityAssetType
 If the utility asset type is a point container, this property is
 the appropriate display scale (zoom level) for displaying the
 container when it is opened. For other utility asset types, this
 property defaults to 0.0.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double containerViewScale;

/** The name of the @c AGSUtilityAssetType
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

/** The @c AGSUtilityTerminalConfiguration of the @c AGSUtilityAssetType
 Only asset types whose network source is @c AGSUtilityNetworkSourceUsageTypeDevice can have a terminal configuration
 @since 100.6
 */
@property (nullable, nonatomic, strong, readonly) AGSUtilityTerminalConfiguration *terminalConfiguration;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
