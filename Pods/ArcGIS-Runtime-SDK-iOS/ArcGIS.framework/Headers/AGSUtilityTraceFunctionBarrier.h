/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTraceFunctionBarrier.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityTraceFunction;

/** @brief An @c AGSUtilityTraceFunctionBarrier stops continued traversal when a comparison expression evaluates as true.

 An @c AGSUtilityTraceFunctionBarrier performs a comparison expression between the current results of an @c AGSUtilityTraceFunction and a given value.
 Remember that an @c AGSUtilityTraceFunction references an @c AGSUtilityNetworkAttribute and a calculation that is applied to it (Min, Max, Count, etc.)
 Once the comparison evaluates as true, network traversal stops.
 @since 100.7
 */
@interface AGSUtilityTraceFunctionBarrier : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a barrier that stops tracing when an @c AGSUtilityTraceFunction compared to a value evaluates to true.
 @param traceFunction The calculation to perform.
 @param comparisonOperator The operator used for the comparison.
 @param value The value to compare against.
 @since 100.7
 */
-(instancetype)initWithTraceFunction:(AGSUtilityTraceFunction *)traceFunction 
                  comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator 
                               value:(id)value;

/** Creates a barrier that stops tracing when an @c AGSUtilityTraceFunction compared to a value evaluates to true.
 @param traceFunction The calculation to perform.
 @param comparisonOperator The operator used for the comparison.
 @param value The value to compare against.
 @since 100.7
 */
+(instancetype)utilityTraceFunctionBarrierWithTraceFunction:(AGSUtilityTraceFunction *)traceFunction 
                                         comparisonOperator:(AGSUtilityAttributeComparisonOperator)comparisonOperator 
                                                      value:(id)value;

#pragma mark -
#pragma mark properties

/** The operator used for the comparison.
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityAttributeComparisonOperator comparisonOperator;

/** The calculation to perform.
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityTraceFunction *traceFunction;

/** The value to compare against.
 The type of this numeric value is dependent on the @c AGSUtilityNetworkAttributeDataType of the @c AGSUtilityNetworkAttribute.
 @since 100.7
 */
@property (nonatomic, strong, readonly) id value;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
