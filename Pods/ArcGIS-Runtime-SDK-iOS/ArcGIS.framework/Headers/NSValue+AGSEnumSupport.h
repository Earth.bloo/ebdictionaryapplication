/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <Foundation/NSValue.h>
#import <ArcGIS/AGSEnumerations.h>

@interface NSValue (AGSEnumSupport)

-(AGSMapServiceImageFormat)ags_mapServiceImageFormatValue;
-(AGSTileImageFormat)ags_tileImageFormatValue;
-(AGSRelationshipConstraintViolationType)ags_relationshipConstraintViolationTypeValue;
-(AGSUtilityTraceResultType)ags_utilityTraceResultTypeValue;

/** Creates an @c NSValue with an @c AGSPortalItemType
 @param type The @c AGSPortalItemType that you want to create the @c NSValue with
 @since 100.7
 */
+(instancetype)ags_valueWithPortalItemType:(AGSPortalItemType)type;

/** Returns the value inside this @c NSValue instance as an @c AGSPortalItemType
 @since 100.7
 */
-(AGSPortalItemType)ags_portalItemTypeValue;

@end
