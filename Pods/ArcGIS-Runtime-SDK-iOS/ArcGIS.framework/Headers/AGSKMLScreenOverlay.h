/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSKMLNode.h>
#import <ArcGIS/AGSColor.h>

@class AGSKMLIcon;
@class AGSKMLImageCoordinate;

/** @file AGSKMLScreenOverlay.h */ //Required for Globals API doc

/** @brief A KML Screen Overlay element
 
 An instance of this class represents a KML `<ScreenOverlay>` element which
 draws an image overlay at a fixed screen location.
 Screen overlays may be used for compasses, logos and heads-up displays.
 
 @since 100.4
 */
@interface AGSKMLScreenOverlay : AGSKMLNode

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

- (instancetype)init NS_UNAVAILABLE;

/** Creates a screen overlay with the specified icon.
 @param icon The icon associated to the screen overlay.
 @since 100.7
 */
-(instancetype)initWithIcon:(nullable AGSKMLIcon *)icon;

/** Creates a screen overlay with the specified icon.
 @param icon The icon associated to the screen overlay.
 @since 100.7
 */
+(instancetype)KMLScreenOverlayWithIcon:(nullable AGSKMLIcon *)icon;

#pragma mark -
#pragma mark properties

/** A color mask that is used to blend the image associated with screen overlay.
 @note Pixels in the overlay image are multiplied by this color channel-by-channel.
 @since 100.4
 */
@property (nullable, nonatomic, strong, readwrite) AGSColor *color;

/** The KML screen overlay's draw order. It defines the stacking order for the images in overlapping overlays.
 Overlays with higher drawOrder values are drawn on top of overlays with lower drawOrder values.
 @since 100.4
 */
@property (nonatomic, assign, readwrite) NSInteger drawOrder;

/** The KML scene overlay's icon.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLIcon *icon;

/** Specifies a point on (or outside of) the overlay image that is mapped to the screen coordinate (@c AGSKMLScreenOverlay#screenCoordinate). It requires x and y values, and the units for those values.
 The x and y values can be specified in three different ways: as pixels ("pixels"), as fractions of the image ("fraction"), or as inset pixels ("insetPixels"), which is an offset in pixels from the upper right corner of the image.
 The x and y positions can be specified in different ways—for example, x can be in pixels and y can be a fraction.
 The origin of the coordinate system is in the lower left corner of the image.
 If not specified, the default is the center of the overlay image.
 @see @c AGSKMLScreenOverlay#screenCoordinate
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLImageCoordinate *overlayCoordinate;

/** Indicates the angle of rotation of the parent object. A value of 0 means no rotation.
 The value is an angle in degrees counter clockwise starting from north.
 The center of the rotation, if not specified in rotation coordinate (@c AGSKMLScreenOverlay#rotationCoordinate), is the center of the overlay image.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) double rotation;

/** Point relative to the screen about which the screen overlay is rotated.
 If not specified, the rotation point is the center of the screen.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLImageCoordinate *rotationCoordinate;

/** Specifies a point relative to the screen origin that the overlay image is mapped to.
 The x and y values can be specified in three different ways: as pixels ("pixels"), as fractions of the screen ("fraction"), or as inset pixels ("insetPixels"), which is an offset in pixels from the upper right corner of the screen.
 The x and y positions can be specified in different ways—for example, x can be in pixels and y can be a fraction.
 The origin of the coordinate system is in the lower left corner of the screen.
 If not specified, the default is the center of the screen.
 @see @c AGSKMLScreenOverlay#overlayCoordinate
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLImageCoordinate *screenCoordinate;

/** Specifies the size of the image for the screen overlay.
 For example:
 A x value of -1 and y value of -1 in fractions indicates the image size equals its original x and y dimensions.
 A x value of -1 and y value of 0.2 in fractions indicates the image size equals its original x dimension and 20% of its y dimension.
 A x value of 100 and y value of 500 in pixels indicates the image is of size 100 pixels by 500 pixels.
 If not specified, the default is the original size of the image.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSKMLImageCoordinate *size;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END

@end
