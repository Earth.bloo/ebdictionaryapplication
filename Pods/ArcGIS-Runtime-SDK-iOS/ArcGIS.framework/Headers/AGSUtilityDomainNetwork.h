/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityDomainNetwork.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityNetworkSource;
@class AGSUtilityTier;
@class AGSUtilityTierGroup;

/** The type of subnetwork controllers that are supported by a domain network
 @see @c AGSUtilityDomainNetwork
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilitySubnetworkControllerType) {
    AGSUtilitySubnetworkControllerTypeNone = 0,    /*!< This domain network doesn't support subnetworks. */
    AGSUtilitySubnetworkControllerTypeSource = 1,  /*!< Subnetwork controllers within this domain network serve as sources. */
    AGSUtilitySubnetworkControllerTypeSink = 2     /*!< Subnetwork controllers within this domain network serve as sinks. */
};

/** Describes the tier type of a domain network.
 @see @c AGSUtilityDomainNetwork
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilityTierType) {
    AGSUtilityTierTypeHierarchical = 1,  /*!< Tiers within this domain network are hierarchical. */
    AGSUtilityTierTypePartitioned = 2    /*!< Tiers within this domain network are partitioned. */
};

/** @brief A domain network inside a utility network

 Domain networks organize features in a utility network based on what
 utility service they provide, such as natural gas, water, electricity,
 or structural elements such as poles or conduits. Each utility network
 will have a single structure network and one or more domain networks for
 the actual utility services they provide.

 @c AGSUtilityDomainNetwork objects can be obtained from the utility network definition 
 using the @c AGSUtilityNetworkDefinition#domainNetworks property.
 @since 100.7
 */
@interface AGSUtilityDomainNetwork : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The display (non-normalized) name of the @c AGSUtilityDomainNetwork
 This name is suitable for display to end-users.
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSString *alias;

/** The ID of the @c AGSUtilityDomainNetwork
 @since 100.7
 */
@property (nonatomic, assign, readonly) NSInteger domainNetworkID;

/** The normalized name of the @c AGSUtilityDomainNetwork
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSString *name;

/** The collection of @c AGSUtilityNetworkSource objects in this @c AGSUtilityDomainNetwork
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityNetworkSource *> *networkSources;

/** Whether this @c AGSUtilityDomainNetwork is the structure network
 @since 100.7
 */
@property (nonatomic, assign, readonly, getter=isStructureNetwork) BOOL structureNetwork;

/** The type of subnetwork controller supported in this @c AGSUtilityDomainNetwork
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilitySubnetworkControllerType subnetworkControllerType;

/** A collection of all of the @c AGSUtilityTierGroup objects for this domain network
 Only hierarchical domain networks support tier groups. If no tier groups exist, an empty list is returned.
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityTierGroup *> *tierGroups;

/** A collection of all of the @c AGSUtilityTier objects for this domain network
 @since 100.7
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityTier *> *tiers;

/** The tier type of this domain network
 Valid values in the @c AGSUtilityTierType enum are Hierarchical (typically used with pressure networks) and Partitioned (typically used with electrical networks).
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityTierType tierType;

#pragma mark -
#pragma mark methods

/** Gets the @c AGSUtilityTierGroup object with the specified name
 @param name The name of the desired @c AGSUtilityTierGroup object to return.
 @return An @c AGSUtilityTierGroup object with the specified name.
 @since 100.7
 */
-(nullable AGSUtilityTierGroup *)tierGroupWithName:(NSString *)name;

/** Gets the @c AGSUtilityTier object with the specified name
 @param name The name of the desired @c AGSUtilityTier object to return.
 @return An @c AGSUtilityTier object with the specified name.
 @since 100.7
 */
-(nullable AGSUtilityTier *)tierWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
