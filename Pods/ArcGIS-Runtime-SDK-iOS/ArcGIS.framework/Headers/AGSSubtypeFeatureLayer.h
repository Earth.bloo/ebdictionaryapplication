/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSSubtypeFeatureLayer.h */ //Required for Globals API doc

#import <ArcGIS/AGSFeatureLayer.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSArcGISFeatureTable;
@class AGSSubtypeSublayer;

/** @brief A layer that can visualize feature data with different visibility, rendering, popup properties,
 and so on for some or all of the @c AGSFeatureSubtype in an @c AGSArcGISFeatureTable.  The
 @c AGSArcGISFeatureTable must have an @c AGSArcGISFeatureLayerInfo#subtypeField defined, and at
 least one @c AGSFeatureSubtype defined, otherwise the layer will fail to load.

 @see @c AGSFeatureLayer
 @since 100.7
 */
@interface AGSSubtypeFeatureLayer : AGSFeatureLayer

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a new subtype feature layer object.
 If are there no @c AGSArcGISFeatureTable#featureSubtypes objects defined on the
 @c AGSArcGISFeatureTable, the layer will fail to load.
 @param featureTable The feature table used as the source of the subtype feature layer
 @since 100.7
 */
-(instancetype)initWithFeatureTable:(AGSArcGISFeatureTable *)featureTable;

/** Creates a new subtype feature layer object.
 If are there no @c AGSArcGISFeatureTable#featureSubtypes objects defined on the
 @c AGSArcGISFeatureTable, the layer will fail to load.
 @param featureTable The feature table used as the source of the subtype feature layer
 @since 100.7
 */
+(instancetype)subtypeFeatureLayerWithFeatureTable:(AGSArcGISFeatureTable *)featureTable;

#pragma mark -
#pragma mark properties

/** The mutable collection of @c AGSSubtypeSublayer
 The objects in this collection are the same objects in @c AGSLayerContent#subLayerContents.
 The difference between the two collections is this collection is modifyable so the layer
 order is configurable.
 @note This array does not allow duplicate objects.
 @since 100.7
 */
@property (nonatomic, strong, readonly) NSMutableArray<AGSSubtypeSublayer *> *subtypeSublayers;

#pragma mark -
#pragma mark methods

/** Finds an @c AGSSubtypeSublayer for a feature subtype based on the @c AGSFeatureSubtype#code
 When working with a particular @c AGSArcGISFeature, the @c AGSFeatureSubtype#code is the value
 of the @c AGSArcGISFeatureLayerInfo#subtypeField.  The corresponding @c AGSSubtypeSublayer
 can be found using the value of that field.
 @param code The subtype code of the sublayer to retrieve
 @return The @c AGSSubtypeSublayer of the subtype code
 @since 100.7
 */
-(nullable AGSSubtypeSublayer *)sublayerWithCode:(id)code;

/** Finds an @c AGSSubtypeSublayer for a feature subtype based on the subtype name
 @param name The subtype name of the sublayer to retrieve
 @return The @c AGSSubtypeSublayer of the subtype name
 @since 100.7
 */
-(nullable AGSSubtypeSublayer *)sublayerWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
