/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTraceConfiguration.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityDomainNetwork;
@class AGSUtilityTier;
@class AGSUtilityTraversability;
@class AGSUtilityPropagator;

/** @brief The set of utility network parameters that define elements of a trace or
 of a subnetwork

 @since 100.7
 */
@interface AGSUtilityTraceConfiguration : AGSObject

#pragma mark -
#pragma mark initializers

/** Creates an @c AGSUtilityTraceConfiguration with default values
 @since 100.7
 */
-(instancetype)init;

/** Creates an @c AGSUtilityTraceConfiguration with default values
 @since 100.7
 */
+(instancetype)utilityTraceConfiguration;

#pragma mark -
#pragma mark properties

/** The @c AGSUtilityDomainNetwork from which to start the trace.
The @c AGSUtilityDomainNetwork property is required and only used with subnetwork-based traces (@c AGSUtilityTraceTypeSubnetwork, @c AGSUtilityTraceTypeUpstream, etc.)
@since 100.7
*/
@property (nullable, nonatomic, strong, readwrite) AGSUtilityDomainNetwork *domainNetwork;

/** Indicates whether to ignore barriers at starting points.
 The default is @c NO.
 One example of how this can be used is with an upstream protective device trace.  The first call to the trace will return the first upstream device
 that meets the protective device criteria.  To find the next device upstream, you would set a starting point on the device returned by the first trace.
 With IgnoreBarriersAtStartingPoints set to @c NO, this second trace would immediately stop at the starting point, since it meets the trace termination criteria.
 Setting this option to @c YES will allow the trace to ignore the starting point, and continue to the subsequent device upstream.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL ignoreBarriersAtStartingPoints;

/** Whether to include barriers in the trace results or
 subnetwork
 The default is @c YES.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL includeBarriers;

/** Whether to include containment features in the trace results
 or subnetwork
 The default is @c NO.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL includeContainers;

/** Whether to include content in the trace results or subnetwork
 The default is @c NO.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL includeContent;

/** Whether to include structure features in the trace results or
 subnetwork
 The default is @c NO.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL includeStructures;

/** A collection of @c AGSUtilityPropagator objects to execute while performing the trace.
 Propagator objects allow a subset of @c AGSUtilityNetworkAttribute values to propagate through a network while executing a trace.
 For example, in phase propagation, open devices along the network will restrict some phases from continuing along the trace.
 Propagators only apply to subnetwork-based traces. (@c AGSUtilityTraceTypeUpstream, @c AGSUtilityTraceTypeDownstream, and so on).
 @since 100.7
 */
@property (nonatomic, copy, readwrite) NSArray<AGSUtilityPropagator *> *propagators;

/** The @c AGSUtilityTier that is used as the start of the trace.
 This property is only used with subnetwork-based traces (subnetwork upstream, etc.). 
 If @c AGSUtilityTraceConfiguration#domainNetwork represents a partitioned network, this property is optional. 
 If not @c nil, the Trace routines will perform an additional check to validate that the starting points and barriers belong to this tier.
 If @c AGSUtilityTraceConfiguration#domainNetwork represents a hierarchical network, this property is required. 
 Since rows in hierarchical networks can belong to multiple tiers, this property tells the subnetwork tracer which tier to use for tracing.
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSUtilityTier *sourceTier;

/** The @c AGSUtilityTier that is used to constrain the tiers returned by the upstream and downstream traces.
 This property is optional. 
 If @c nil, the upstream and downstream traces will stop in the current tier.
 If a target tier is specified, the trace will continue upstream or downstream into the specified tier (inclusive).
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSUtilityTier *targetTier;

/** The traversability conditions for the trace
 @since 100.7
 */
@property (nullable, nonatomic, strong, readwrite) AGSUtilityTraversability *traversability;

/** Indicates whether to validate network consistency as part of the trace operation.
 If set to @c YES, trace operations will fail if dirty areas are encountered during the trace.  The default is @c YES.
 @since 100.7
 */
@property (nonatomic, assign, readwrite) BOOL validateConsistency;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
