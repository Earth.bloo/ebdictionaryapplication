/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityAssociation.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityElement;

/** An enumeration of the various types of associations supported by the utility network.
 @since 100.7
 */
typedef NS_ENUM(NSInteger, AGSUtilityAssociationType) {
    AGSUtilityAssociationTypeConnectivity = 1,  /*!< The association represents connectivity between two junctions. */
    AGSUtilityAssociationTypeContainment = 2,   /*!< The association represents containment of one @c AGSUtilityElement within another. */
    AGSUtilityAssociationTypeAttachment = 3     /*!< The association represents a structural attachment. */
};

/** @brief A connectivity, containment, or structural attachment association.

 Associations are an integral part of network topology. Connectivity associations allow connectivity between two junctions that don't have geometric coincidence (are not in the same location). 
 Structural attachment associations allow modeling equipment attached to structures. Containment associations allow modeling containment of features within other features. 
 Network traces make use of associations. Associations are defined using two @c AGSUtilityElement objects.
 @since 100.7
 */
@interface AGSUtilityAssociation : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The @c AGSUtilityAssociationType of this association.
 @since 100.7
 */
@property (nonatomic, assign, readonly) AGSUtilityAssociationType associationType;

/** If this @c AGSUtilityAssociation represents a containment association, returns whether the containment is visible.
 @since 100.7
 */
@property (nonatomic, assign, readonly, getter=isContainmentVisible) BOOL containmentVisible;

/** The first participant in an @c AGSUtilityAssociation.
 If this is a containment association, this property represents the container. If this is a structural attachment association, this property represents the structure.
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityElement *fromElement;

/** The global ID of the association.
 @since 100.7
 */
@property (nonatomic, strong, readonly) NSUUID *globalID;

/** The second participant in an association.
 If this is a containment association, this property represents the content. If this is a structural attachment association, this property represents the attached item.
 @since 100.7
 */
@property (nonatomic, strong, readonly) AGSUtilityElement *toElement;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
