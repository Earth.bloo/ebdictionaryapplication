/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSRenderer.h>

@class AGSDictionarySymbolStyle;

/** @file AGSDictionaryRenderer.h */ //Required for Globals API doc

/** @brief Renders @AGSGeoElements using symbols generated from an @c AGSDictionarySymbolStyle.
 
 The DictionaryRenderer applies symbols to features or graphics according
 to a set of input attribute values. The symbol primitives and logic (rule engine) for
 applying symbols is provided by an associated @c AGSDictionarySymbolStyle.
 The @c AGSDictionarySymbolStyle depends on attribute names to define the
 symbology of each feature. The attributes reference different symbol components
 in the style, which are assembled to create a complex symbol. In order to display
 geoelements using the @c AGSDictionaryRenderer, attributes in your data must either use
 the expected attribute names or be mapped to the proper style attributes.
 @see @c AGSDictionarySymbolStyle
 @since 100.0
 */
@interface AGSDictionaryRenderer : AGSRenderer

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Initialize the renderer with a reference to the dictionary symbol style.
 @param dictionarySymbolStyle to use
 @return a new dictionary renderer
 @since 100
 */
-(instancetype)initWithDictionarySymbolStyle:(AGSDictionarySymbolStyle*)dictionarySymbolStyle;

/** Initialize the renderer with a reference to the dictionary symbol style.
 @param dictionarySymbolStyle to use
 @return a new dictionary renderer
 @since 100
 */
+(instancetype)dictionaryRendererWithDictionarySymbolStyle:(AGSDictionarySymbolStyle*)dictionarySymbolStyle;

/** Initialize the renderer with a reference to the dictionary symbol style.
 @param dictionarySymbolStyle to use
 @param symbologyFieldOverrides specifying the mapping between the field names that @c dictionarySymbolStyle specification expects and the field names in the source data
 @param textFieldOverrides specifying the mapping between the field names that @c dictionarySymbolStyle specification expects and the field names in the source data
 @return a new dictionary renderer
 @since 100
 */
-(instancetype)initWithDictionarySymbolStyle:(AGSDictionarySymbolStyle*)dictionarySymbolStyle
                     symbologyFieldOverrides:(NSDictionary<NSString*, id> *)symbologyFieldOverrides
                          textFieldOverrides:(NSDictionary<NSString*, NSString*> *)textFieldOverrides;

/** Initialize the renderer with a reference to the dictionary symbol style.
 @param dictionarySymbolStyle to use
 @param symbologyFieldOverrides specifying the mapping between the field names that @c dictionarySymbolStyle specification expects and the field names in the source data
 @param textFieldOverrides specifying the mapping between the field names that @c dictionarySymbolStyle specification expects and the field names in the source data
 @return a new dictionary renderer
 @since 100
 */
+(instancetype)dictionaryRendererWithDictionarySymbolStyle:(AGSDictionarySymbolStyle *)dictionarySymbolStyle
                                   symbologyFieldOverrides:(NSDictionary<NSString*, id> *)symbologyFieldOverrides
                                        textFieldOverrides:(NSDictionary<NSString*, NSString*> *)textFieldOverrides;

#pragma mark -
#pragma mark properties

/** The dictionary symbol style to be applied by the renderer
 @since 100
 */
@property (nonatomic, strong, readwrite) AGSDictionarySymbolStyle *dictionarySymbolStyle;

/** The @c NSDictionary of a symbology attribute and its override used to fetch a symbol from the dictionary symbol style.
 An @c AGSDictionarySymbolStyle uses a collection of expected attribute names to build a symbol based on input values. When
 initialized, fields in the input dataset are mapped to the expected attribute names using case-insensitive matching.
 If your data uses a different name for an expected symbol attribute, use the @c AGSDictionaryRenderer#symbologyFieldOverrides to map an expected field to the
 correct field in your data. You can also exclude an expected field by providing an empty string for it in the overrides.
 @since 100.0
 */
@property (nonatomic, copy, readwrite) NSDictionary<NSString*, NSString*> *symbologyFieldOverrides;

/** The @c NSDictionary of a text attribute and its override used to fetch symbol text and placement from the dictionary symbol style.
 An @c AGSDictionarySymbolStyle uses a collection of expected attribute names to build a symbol based on input values. When
 initialized, fields in the input dataset are mapped to the expected attribute names using case-insensitive matching.
 If your data uses a different name for an expected text attribute, use the @c AGSDictionaryRenderer#textFieldOverrides to map an expected field to the
 correct field in your data. You can also exclude an expected field by providing an empty string for it in the overrides.
 @since 100.0
 */
@property (nonatomic, copy, readwrite) NSDictionary<NSString*, NSString*> *textFieldOverrides;

#pragma mark -
#pragma mark methods

@end

@interface AGSDictionaryRenderer (AGSDeprecated)

/** The maximum scale (2D) or distance (3D) at which the symbol's text will be visible.
 @since 100.0
 @deprecated 100.6.0. Use the AGSDictionarySymbolStyleConfiguration settings instead.
 */
@property (nonatomic, assign, readwrite) double textVisibilityMaxScale __deprecated_msg("Use the AGSDictionarySymbolStyleConfiguration settings instead.");

/** The minimum scale (2D) or distance (3D) at which the symbol's text will be visible.
 @since 100.0
 @deprecated 100.6.0. Use the AGSDictionarySymbolStyleConfiguration settings instead.
 */
@property (nonatomic, assign, readwrite) double textVisibilityMinScale __deprecated_msg("Use the AGSDictionarySymbolStyleConfiguration settings instead.");

/** The visibility for the text on the symbol.
 @since 100.0
 @deprecated 100.6.0. Use the AGSDictionarySymbolStyleConfiguration settings instead.
 */
@property (nonatomic, assign, readwrite, getter=isTextVisible) BOOL textVisible __deprecated_msg("Use the AGSDictionarySymbolStyleConfiguration settings instead.");

@end

NS_ASSUME_NONNULL_END
