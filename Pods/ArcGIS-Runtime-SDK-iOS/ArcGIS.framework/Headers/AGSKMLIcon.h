/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @file AGSKMLIcon.h */ //Required for Globals API doc

/** @brief The KML icon of the ground and screen overlays.
 
 The KML icon of the ground and screen overlays.
 
 @since 100.6
 */
@interface AGSKMLIcon : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Initialize this icon with URL to an image file on the web, on disk or relative path (for example, @c [NSURL URLWithString:@"files/abc.png"] or @c [NSURL URLWithString:@"abc.png"]) to an image file on disk.
 @param URL to an image file on the web, on disk or relative path to an image file on disk.
 @return A new kml icon
 @since 100.6
 */
-(instancetype)initWithURL:(NSURL*)URL;

/** Initialize this icon with URL to an image file on the web, on disk or relative path (for example, @c [NSURL URLWithString:@"files/abc.png"] or @c [NSURL URLWithString:@"abc.png"]) to an image file on disk.
 @param URL to an image file on the web, on disk or relative path to an image file on disk.
 @return A new kml icon
 @since 100.6
 */
+(instancetype)KMLIconWithURL:(NSURL*)URL;

#pragma mark -
#pragma mark properties

/** The URL of the KML icon.
 @since 100.6
 */
@property (nullable, nonatomic, strong, readonly) NSURL *URL;

/** The KML icon's refresh mode.
 @see @c AGSKMLRefreshMode, @c AGSKMLIcon#refreshInterval
 @since 100.6
 */
@property (nonatomic, assign, readwrite) AGSKMLRefreshMode refreshMode;

/** The KML icon refresh interval, in seconds.
 @see @c AGSKMLRefreshMode
 @since 100.6
 */
@property (nonatomic, assign, readwrite) NSTimeInterval refreshInterval;

/** The KML icon's view refresh mode.
 @see @c AGSKMLViewRefreshMode, @c AGSKMLIcon#viewRefreshTime
 @since 100.6
 */
@property (nonatomic, assign, readwrite) AGSKMLViewRefreshMode viewRefreshMode;

/** The KML icon view refresh time, in seconds.
 @see @c AGSKMLViewRefreshMode
 @since 100.6
 */
@property (nonatomic, assign, readwrite) NSTimeInterval viewRefreshTime;

@end

NS_ASSUME_NONNULL_END
