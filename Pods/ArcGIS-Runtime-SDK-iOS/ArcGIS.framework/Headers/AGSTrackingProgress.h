/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSTrackingProgress.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSPolyline;
@class AGSTrackingDistance;

/** @brief Defines tracking progress (passed and remaining geometries, remaining time and distance)

 @since 100.6
 */
@interface AGSTrackingProgress : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The distance still to be traversed
 @see @c AGSTrackingDistance
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSTrackingDistance *remainingDistance;

/** The polyline geometry still to be traversed
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSPolyline *remainingGeometry;

/** The time to traverse remaining geometry/distance
 The time in minutes.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double remainingTime;

/** The polyline geometry already traversed
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSPolyline *traversedGeometry;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
