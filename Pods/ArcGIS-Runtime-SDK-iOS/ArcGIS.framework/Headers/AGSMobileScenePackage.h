/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSLoadableBase.h>

@class AGSLocatorTask;
@class AGSScene;
@class AGSItem;
@class AGSExpiration;

@protocol AGSCancelable;

NS_ASSUME_NONNULL_BEGIN

/** @file AGSMobileScenePackage.h */ //Required for Globals API doc

/** @brief A mobile scene package
 
 Instances of this class represent a mobile scene package (.mspk file).
 
 A mobile scene package can be created from ArcGIS Pro. It is a transport mechanism for scenes,
 their layers, and the layer's data. It contains metadata about the package (description,
 thumbnail, etc.), one or more mobile scenes, layers, data, and optionally locators.
 
 @since 100.5
 */

@interface AGSMobileScenePackage : AGSLoadableBase

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Initialize this object with the specified mobile scene package (.mspk file) on disk.
 @param fileURL to the mobile scene package (.mspk file) on disk.
 @return A new mobile scene package object
 @since 100.5
 */
-(instancetype)initWithFileURL:(NSURL *)fileURL;

/** Initialize this object with the name of a mobile scene package (.mspk file),
 excluding the ".mspk" extension, within the application bundle or shared documents directory.
 @param name of the mobile scene package (excluding the .mspk extension)
 @return A new mobile scene package object
 @since 100.5
 */
-(instancetype)initWithName:(NSString *)name;

/** Initialize this object with the specified mobile scene package (.mspk file) on disk.
 @param fileURL to the mobile scene package (.mspk file) on disk.
 @return A new mobile scene package object
 @since 100.5
 */
+(instancetype)mobileScenePackageWithFileURL:(NSURL *)fileURL;

/** Initialize this object with the name of a mobile scene package (.mspk file),
 excluding the ".mspk" extension, within the application bundle or shared documents directory.
 @param name of the mobile scene package (excluding the .mspk extension)
 @return A new mobile scene package object
 @since 100.5
 */
+(instancetype)mobileScenePackageWithName:(NSString *)name;

#pragma mark -
#pragma mark properties

/** Metadata associated with the mobile scene package
 @since 100.5
 */
@property (nullable, nonatomic, strong, readonly) AGSItem *item;

/** The locator contained in the mobile scene package
 @since 100.5
 */
@property (nullable, nonatomic, strong, readonly) AGSLocatorTask *locatorTask;

/** The URL of the mobile scene package (.mspk file) on disk.
 @since 100.5
 */
@property (nonatomic, strong, readonly) NSURL *fileURL;

/** The scenes contained in the mobile scene package
 @since 100.5
 */
@property (nonatomic, copy, readonly) NSArray<AGSScene*> *scenes;

/** Version of the mobile scene package
 @since 100.5
 */
@property (nonatomic, copy, readonly) NSString *version;

/** Expiration details for this mobile map package, if provided. If the package has expired and was authored as @c ExpirationTypePreventExpiredAccess, then it will fail to load and can no longer be used.
 @since 100.5
 */
@property (nullable, nonatomic, strong, readonly) AGSExpiration *expiration;

#pragma mark -
#pragma mark methods

/** Closes a mobile scene package.
 Closes a mobile scene package and frees file locks on the underlying .mspk file or directory.
 
 All references to mobile scene package data (scenes, layers, tables, locators, etc.)
 should be released before closing the package. If active references to mobile scene package
 data exist, this method will still close the package, but subsequent rendering and data
 access methods will fail. Results of accessing mobile scene package data after
 closing it are undefined.
 
 After closing a mobile scene package, the underlying .mspk file or directory can be moved or deleted.
 
 Closing a mobile scene package is not necessary if the package has not been loaded.
 @see @c AGSMobileMapPackage#close, @c AGSGeodatabase#close
 @since 100.6
 */
-(void)close;

/** Unpacks a mobile scene package file (.mspk) to the specified directory.
 Note that unpacking will fail if the package is expired and was authored as @c AGSExpirationTypePreventExpiredAccess.
 @param fileURL location to .mspk file
 @param outputDirectory specifying where to unpack the package. If the last component of the directory
 location does not exist, it will be created during unpacking.
 @param completion block that is invoked when the operation completes.
 @return operation that can be canceled
 @since 100.5
 */
+(id<AGSCancelable>)unpackMobileScenePackageAtFileURL:(NSURL *)fileURL
                                      outputDirectory:(NSURL*)outputDirectory
                                           completion:(void(^)(NSError * __nullable error))completion;

@end

@interface AGSMobileScenePackage (AGSDeprecated)

/** Checks if the package can be read directly without requiring to be unpacked. Always returns true after deprecation in version 100.7.
 - Prior to version 100.7 some data formats could only be accessed if they were present on disk, for example, @c AGSRasterLayer. In these situations, this method would return false and you would need to unpack the package to access the data.
 - From version 100.7 and onwards this limitation has been removed allowing the data to be read directly from the mobile scene package. This method always returns a result of true.
 Since this method is no longer required it can be removed from calling code including any subsequent use of @c AGSMobileScenePackage#unpackWithMobileScenePackageFileURL:outputDirectory:completion:.
 @param fileURL location to .mspk file
 @param completion block that is invoked with information about whether direct read is supported if the operation succeeds, or an error if it fails
 @return operation that can be canceled
 @see @c AGSMobileScenePackage#unpackWithMobileScenePackageFileURL:outputDirectory:completion:
 @since 100.5
 @deprecated 100.7 This method is no longer required as the result is always true. It can be removed from calling code including any subsequent use of @c AGSMobileScenePackage#unpackWithMobileScenePackageFileURL:outputDirectory:completion:.
 */
+(id<AGSCancelable>)checkDirectReadSupportForMobileScenePackageAtFileURL:(NSURL*)fileURL
                                                              completion:(void(^)(BOOL isDirectReadSupported, NSError * __nullable error))completion __deprecated_msg("This method is no longer required as the result is always true. It can be removed from calling code including any subsequent use of @c AGSMobileScenePackage#unpackWithMobileScenePackageFileURL:outputDirectory:completion:.");

@end

NS_ASSUME_NONNULL_END
