/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSSceneViewInteractionOptions.h */ //Required for Globals API doc

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Options to configure Scene View user interactions
 
 Instances of this class provide options to configure the user interactions supported by @c AGSSceneView.
 
 @since 100.6
 */
@interface AGSSceneViewInteractionOptions : NSObject

#pragma mark -
#pragma mark initializers

+(instancetype)sceneViewInteractionOptions;

#pragma mark -
#pragma mark properties

/** Whether all the user interaction is enabled on the @c AGSSceneView.
 Default is @c YES
 @since 100.6
 */
@property (nonatomic, assign, readwrite, getter=isEnabled) BOOL enabled;

/** Whether the user can flick the @c AGSSceneView with a fast pan gesture.
 Default is @c YES
 @since 100.6
 */
@property (nonatomic, assign, readwrite, getter=isFlickEnabled) BOOL flickEnabled;

/** Zoom factor for animated zooming in and out.
 Default is 2.0. Values less than or equal to 1.0 are not supported and will be ignored.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) double zoomFactor;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
