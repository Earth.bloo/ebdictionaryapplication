//
//  AppView.swift
//  TrySwiftUI
//
//  Created by admin on 12/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import SwiftUI

struct AppView: View {
    init(){
       
    
        UITabBar.appearance().barTintColor = UIColor.init(red: 23/255, green: 30/255, blue: 42/255, alpha: 1)
       
    }
    var body: some View {
        TabView{
            
            DictionaryView().tabItem{
                VStack{
                
                    Image(systemName:"book.fill").resizable().frame(width: 60, height: 60)
                Text("Dict")
                }
            }
            ExploreView().tabItem{
                VStack{
                    Image(systemName:"house.fill").resizable().frame(width: 60, height: 60)
                 
                Text("Explorer")
                }
            }
            ContentView().tabItem{
                VStack{
                    Image(systemName:"person.fill").resizable().frame(width: 60, height: 60)
                 
                Text("Profile")
                }
               
            }
            
            }.edgesIgnoringSafeArea([.top,.leading])
            
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
