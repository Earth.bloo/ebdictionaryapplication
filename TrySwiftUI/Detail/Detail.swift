//
//  Detail.swift
//  TrySwiftUI
//
//  Created by admin on 10/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import SwiftUI

struct Detail:View{
    var vocab:Vocab
    
       var body: some View {
        
        VStack(alignment: .leading){
            HStack{
                Text("123")
                Spacer()
                Text("123")
            }.padding(.leading,20)
            .padding(.top,20)
            .padding(.trailing,20)
            HStack{
                Text("123")
                Spacer()
            }
            .padding(.leading,20)
            .padding(.top,20)
            Divider().padding(.all,20)
            
            HStack{
                Text("Example").font(.title)
            }.padding(.leading,20)
            .padding(.trailing,20)
            Spacer()
           
            

        }.navigationBarTitle(Text("123"))
        
                
 
       }
}

struct BackButton:View {
      let label: String
     
      var body: some View {
          Button(action: {
            print("click")
          }) {
              HStack {
                  Image(systemName: "chevron.left")
                  Text(label)
              }
          }
      }
}

