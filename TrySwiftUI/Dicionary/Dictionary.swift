//
//  Dictionary.swift
//  TrySwiftUI
//
//  Created by admin on 25/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//


import SwiftUI
import Alamofire
import UIKit

struct DictionaryView: View {
    @State private var wordInSearch = ""
    @ObservedObject var DictMV = DictViewModel()
    
    init(){
        DictMV.fetchAllWord()   
              let coloredAppearance = UINavigationBarAppearance()
              coloredAppearance.configureWithTransparentBackground()
              coloredAppearance.backgroundColor = .clear
              coloredAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
              coloredAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
              UITableView.appearance().separatorStyle = .none
                UITableView.appearance().backgroundColor = UIColor(red: 31/255, green: 36/255, blue: 45/255, alpha: 1)
              UITableViewCell.appearance().backgroundColor = UIColor(red: 31/255, green: 36/255, blue: 45/255, alpha: 1)
       
    }
    
    var body: some View {
  
        NavigationView{
           
        List{
            
            TextField("", text: $wordInSearch)
                
                .padding([.leading,.trailing],10)
                .modifier(PlaceholderStyle(showPlaceHolder: wordInSearch.isEmpty,
                placeholder: "Enter a words"))
                
            
            ForEach(DictMV.vocab,id:\.vocabId){ vocab in
                 NavigationLink(destination: Detail(vocab: vocab)){

                    postview(vocab: vocab)
                        
                           }
            }.listRowBackground(Color(red: 31/255, green: 36/255, blue: 45/255, opacity: 1.0))
                .background(Color(red: 31/255, green: 36/255, blue: 45/255, opacity: 1.0))
                

            }.navigationBarTitle(Text("Dictionary"))
    
        }
        
}
}

struct postview:View {
    let vocab:Vocab
    var body: some View{
        
        ZStack{
                   Rectangle().frame(width: 380, height: 60)
                     
                    .foregroundColor(Color.gray)
                    .opacity(0.4)
                    .shadow(radius: 10)
                    .cornerRadius(10)
                    .shadow(radius: 20)
                 
            HStack{
                Text(vocab.vocabName).font(.title)
                        .foregroundColor(.white)
                        .padding(.leading, 18)
                        
                Spacer()
                Text(vocab.typeWord)
                    .foregroundColor(.gray)
                    .padding(.trailing, 20)
       
            }
               }
    
            }
            
        }
        
        
//        HStack{
//            VStack(alignment: .leading){
//                Text(vocab.word)
//                .font(.headline)
//
//                Text(vocab.typeword)
//                    .font(.subheadline)
//            }
//            Spacer()
//            Text("1")
//
//        }.frame(height:60)
        
        
       
           





struct Dictionary_Previews: PreviewProvider {
    static var previews: some View {
        DictionaryView()
    }
}


struct PlaceholderStyle: ViewModifier {
    var showPlaceHolder: Bool
    var placeholder: String

    public func body(content: Content) -> some View {
        ZStack(alignment: .leading) {
            if showPlaceHolder {
                Text(placeholder)
                .padding(.horizontal, 15)
                .foregroundColor(.gray)
                .font(.system(size:17))
                
                
            }
            content
            .frame(height:50)
            .font(.system(size:18))
            .foregroundColor(.white)
            .overlay(RoundedRectangle(cornerRadius: 20)
            .stroke(Color.white, lineWidth: 0.7))
            
        }
    }
}





extension UINavigationController {
        override open func viewDidLoad() {
            super.viewDidLoad()
      
                     
        
            let Apperance = UINavigationBarAppearance()
         
            
            Apperance.configureWithTransparentBackground()
            Apperance.backgroundColor = .clear
            Apperance.backgroundColor = UIColor(red: 31/255, green: 36/255, blue: 45/255, alpha: 1)
           // Apperance.backgroundColor = UIColor(red: 18/255, green: 23/255, blue: 36/255, alpha: 1)
            Apperance.titleTextAttributes = [.foregroundColor: UIColor.white]
            Apperance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
           // UINavigationBar.appearance().standardAppearance = Apperance2
            UINavigationBar.appearance().scrollEdgeAppearance = Apperance
            UINavigationBar.appearance().standardAppearance = Apperance
            UINavigationBar.appearance().scrollEdgeAppearance = Apperance
            UINavigationBar.appearance().tintColor = .white
           


           
        }
    }



