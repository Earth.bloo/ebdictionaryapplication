//
//  DictViewModel.swift
//  TrySwiftUI
//
//  Created by admin on 17/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation
import Alamofire

class DictViewModel:ObservableObject {
    @Published var vocab = [Vocab]()
    
    func fetchAllWord(){
        
        Alamofire.request("http://localhost:5000/api/vocab", method: .get).responseData{ response in
                
                switch(response.result) {
                case.success:
                    do{
                    print(response.result)
                    let decoder = JSONDecoder()
                    self.vocab = try decoder.decode([Vocab].self, from: response.value!)
                        print(self.vocab)
                    }catch{
                             print(String(describing: error))
                                                       
                                                   }
                   
               case.failure(let error):
                    print("Not Success",error)
                    
                }
                print("finish")

            }
        
    }
    
}
