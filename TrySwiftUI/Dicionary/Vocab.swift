//
//  Vocab.swift
//  TrySwiftUI
//
//  Created by admin on 25/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import Foundation

struct Vocab:Decodable{
    var vocabId:Int!
    var vocabName:String!
    var pronunciation:String!
    var typeWord:String!
    var defination:String!
    var example:String?
    var synonym:String?
}



