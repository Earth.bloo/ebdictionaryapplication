//
//  ContentView.swift
//  TrySwiftUI
//
//  Created by admin on 10/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import SwiftUI
import ArcGIS



struct ContentView: View {
    
    
    let post = ["1","2"]
    let genre = ["Menu1","Menu2","Menu3"]
    var body: some View {
        
        NavigationView{
            List{
                   
                        ForEach(genre,id: \.self){ gen in
                             genreView(gen: gen)
                        }                        
                        }.navigationBarTitle(Text("Hello"))
         
                
                
               // mapView().frame( height: 300)
               
                
                
                
            }
               
            
                
        }
    
}





struct genreView:View {
    let gen:String
    var body:some View{
        ZStack{
            Rectangle().frame(width: 370, height: 130)
                .foregroundColor(Color.gray)
                .shadow(radius: 10)
                .cornerRadius(20)
                .padding(.all,10)
                
            Text(gen).bold().foregroundColor(.white)
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
