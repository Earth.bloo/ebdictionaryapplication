//
//  ExploreView.swift
//  TrySwiftUI
//
//  Created by admin on 22/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import SwiftUI

struct ExploreView: View {
    let MySet = ["Set1","Set2","Set3","Set4","Set5","Set6"]
    let TOEICSet = ["TOEICSet1","TOEICSet2","TOEICSet3","TOEICSet4","TOEICSet5","TOEICSet6"]
    @ObservedObject var ExploreVM = ExploreViewModel()
    init(){
        let user_id = 1
        
        ExploreVM.fetchFloder(user_id: user_id)
    }
    var body: some View {
        
            List{
                VStack(alignment: .leading){
                Text("HELLOOOO")
                    .font(.system(size: 30)).bold()
                    .padding(.top, 20)
                    .padding(.leading, 6)
                    .foregroundColor(.white)
                    
                ScrollView(.horizontal, showsIndicators: false){
                    HStack{
                        ForEach(ExploreVM.floder,id:\.floderId){ set in
                             setView(floder: set)
                        }

                    }
                }
                Text("TOEIC")
                    .font(.system(size: 30)).bold()
                    .padding(.top, 20)
                    .padding(.leading, 6)
                    .foregroundColor(.white)
                ScrollView(.horizontal, showsIndicators: false){
                        HStack{
                            ForEach(TOEICSet,id: \.self){ set in
                                 setView2(set: set)
                            }

                        }
                    }
//               r
                }
                
               
               
        }
    }
}
    
    
struct setView:View {
    let floder:Floder
    var body:some View{
        VStack(alignment: .leading){
        ZStack{
            
            Rectangle().frame(width: 150, height: 150)
                .foregroundColor(Color.red)
                .shadow(radius: 10)
                .cornerRadius(10)
                
                
            Text(floder.floderName).bold().foregroundColor(.white)
        }
            Text(floder.floderName)
                .font(.system(size: 20))
                .bold()
                .foregroundColor(.white)
                .padding(.top, 3)
        }.padding(.all,10)
    }
}

struct setView2:View {
    let set:String
    var body:some View{
        VStack(alignment: .leading){
        ZStack{
            
            Rectangle().frame(width: 150, height: 150)
                .foregroundColor(Color.red)
                .shadow(radius: 10)
                .cornerRadius(10)
                
                
            Text("Sets").bold().foregroundColor(.white)
        }
            Text("Text")
                .font(.system(size: 20))
                .bold()
                .foregroundColor(.white)
                .padding(.top, 3)
        }.padding(.all,10)
    }
}


struct ExploreView_Previews: PreviewProvider {
    static var previews: some View {
        ExploreView()
    }
}

