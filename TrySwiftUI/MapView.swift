//
//  MapView.swift
//  TrySwiftUI
//
//  Created by admin on 11/5/2563 BE.
//  Copyright © 2563 EarthBlooProduction. All rights reserved.
//

import SwiftUI
import ArcGIS
import MapKit

struct mapView:UIViewRepresentable{
   
    
    func makeUIView(context: UIViewRepresentableContext<mapView>) -> AGSMapView{
        let mapView:AGSMapView = AGSMapView()
        let map = AGSMap(basemapType: .streets, latitude: 13.00, longitude: 100.00, levelOfDetail: 10)
        mapView.map = map
        return mapView
       
    }
    
    func updateUIView(_ uiView: AGSMapView, context: UIViewRepresentableContext<mapView>) {
        
    }
  

  
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        mapView()
    }
}
